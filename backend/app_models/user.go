package app_models

import (
	"github.com/jinzhu/gorm"
	"time"
	"container/list"
	"math"
	"geek-pixel/backend/pixel_room"
	"fmt"
	"strconv"
	"geek-pixel/backend/helpers"
	"github.com/asaskevich/govalidator"
	"reflect"
)

type User struct {
	gorm.Model
	Name        		string

	Aka 				string  		`gorm:"size:2000";sql:"json"`

	Avatar      		string  		`gorm:"size:2000";sql:"json"`

	Email       		string
	RefLink       		string

	IsOnline    		bool
	IsAdmin    			bool
	IsInactive    		bool
	IsNotJoinToRefer	bool

	PixelsCount     	int
	ManualPixelsCount   int

	LastOnlineAt		*time.Time

	Refer       		*User   			`gorm:"ForeignKey:ReferID;save_associations:false"`
	ReferID     		uint				`gorm:"index"`

	Clan       			*Clan   			`gorm:"ForeignKey:ClanID;save_associations:false"`
	ClanID     			uint				`gorm:"index"`

	City       			*City   			`gorm:"ForeignKey:CityID;save_associations:false"`
	CityID     			uint				`gorm:"index"`

	World          		*World    			`gorm:"ForeignKey:WorldID;save_associations:false"`
	WorldID        		uint     			`gorm:"index"`

	Roles  				[]UserRole    		`gorm:"ForeignKey:UserID;save_associations:false"`
	Pixels  			[]Pixel    			`gorm:"ForeignKey:UserID;save_associations:false"`
	Wallets  			[]Wallet    		`gorm:"ForeignKey:UserID;save_associations:false"`
	UserSocials  		[]UserSocial    	`gorm:"ForeignKey:UserID;save_associations:false"`
	UserProgresses  	[]UserProgress    	`gorm:"ForeignKey:UserID;save_associations:false"`
	UserLogs  			[]UserLog    		`gorm:"ForeignKey:UserID"`
}

func (user *User) CreateClan(db *gorm.DB, name string) Clan {
	var available_action AvailableUserAction
	db.Where("user_id = ?", user.ID).Where("action_type = ?", "add_clan").First(&available_action)

	if (available_action.Value < 1) {
		panic("not_available")
	}

	clan := Clan{Name: name, WorldID: user.WorldID}
	db.Save(&clan)
	user.ClanID = clan.ID
	db.Save(&user)

	var owner = Role{
		Name: "Clan Owner",
		Key: "owner",
		Level: "clan",
		ClanID: user.ClanID,
	}
	db.Create(&owner)

	user.AddRole(db, &owner)

	return clan
}

func (user *User) CreateCity(db *gorm.DB, name string, centerX int, centerY int) {
	var available_action AvailableUserAction
	db.Where("user_id = ?", user.ID).Where("action_type = ?", "add_city").First(&available_action)

	if(available_action.Value < 1){
		panic("not_available")
	}

	go func(){
		defer func(){
			if r := recover(); r != nil {
				pixel_room.SendError(int(user.ID), r)
			}
		}()

		var pixel_size = user.World.GetSettingIntegerValue(db, "pixel_size")

		centerX -= centerX % pixel_size
		centerY -= centerY % pixel_size

		var city_radius = user.World.GetSettingIntegerValue(db, "base_city_radius")

		var emptyPixelType  = PixelType{}
		db.First(&emptyPixelType, "`key` = ?", "grey")

		var city_pixels list.List

		var r = city_radius * pixel_size
		for y := centerY - r; y < centerY + r; y += pixel_size {
			for x := centerX - r; x < centerX + r; x += pixel_size {

				dx := centerX - x; dx *= dx;
				dy := centerY - y; dy *= dy;

				if(math.Sqrt( float64(dx + dy) ) <= float64(r)){
					if(!user.CanAddPixel(db, x, y, emptyPixelType.ID)){
						fmt.Println("x: " + strconv.Itoa(x) + ", y: " + strconv.Itoa(y))
						panic("not_permitted:add_city")
					}
					city_pixels.PushBack(Pixel{ X: x, Y: y })
				}
			}
		}

		var city = City{
			Name: name,
			OwnerID: user.ID,
			WorldID: user.WorldID,
			ClanID: user.ClanID,
			X: centerX,
			Y: centerY,
			UsersCount: 1,
			OnlineUsersCount: 1,
		}
		db.Create(&city)

		user.CityID = city.ID
		db.Save(&user)

		var owner = Role{
			Name: "City Owner",
			Key: "owner",
			Level: "city",
			CityID: user.CityID,
		}
		db.Create(&owner)

		user.AddRole(db, &owner)

		pixel_room.AddCity(int(user.ID), city)

		for e := city_pixels.Front(); e != nil; e=e.Next() {
			var pixel_to_add = e.Value.(Pixel)
			user.AddPixel(db, pixel_to_add.X, pixel_to_add.Y, emptyPixelType.ID, true)
		}

		var created_pixels = []Pixel{}
		db.Model(&city).Related(&created_pixels, "Pixels")

		for index, pixel := range created_pixels {
			_ = index
			pixel.CheckCityEdge(db)
		}

		city.CreateChat(db, user)

		city.IsCreated = true
		db.Save(&city)

		user.AddLog(db, "add_city", city)

		pixel_room.AddCity(int(user.ID), city)

		available_action.Decrement()
		db.Save(&available_action)

		city.CachePixelsCount(db, user)
	}()
}

func (user *User) AddPixel (db *gorm.DB, x int, y int, typeID uint, conditions ...bool) Pixel {

	var cityCreateMode = false
	if (len(conditions) > 0) {
		cityCreateMode = conditions[0]
	}

	var available_action AvailableUserAction
	if(!cityCreateMode){
		db.Where("user_id = ?", user.ID).Where("action_type = ?", "add_pixel").First(&available_action)

		if (available_action.Value < 1) {
			panic("not_available")
		}
	}
	var pixel = Pixel{}

	var pixel_size = user.World.GetSettingIntegerValue(db, "pixel_size")
	pixel.X = x - x % pixel_size
	pixel.Y = y - y % pixel_size
	pixel.WorldID = user.WorldID

	if(cityCreateMode){
		pixel.IsCityInner = true;
	}

	if(!cityCreateMode && !user.CanAddPixel(db, pixel.X, pixel.Y, typeID)){
		panic("not_permitted")
	}

	var exist_pixel Pixel
	db.Where("x = ?", pixel.X).Where("y = ?", pixel.Y).Where("world_id = ?", pixel.WorldID).First(&exist_pixel)

	var previous_pixel_city_id = exist_pixel.CityID

	if(exist_pixel.ID == 0){
		pixel.TypeID = typeID

		pixel.UserID = user.ID
		pixel.CityID = user.CityID
		pixel.ClanID = user.ClanID
		db.Create(&pixel)
	} else {
		exist_pixel.TypeID = typeID
		exist_pixel.IsCityInner = pixel.IsCityInner

		exist_pixel.UserID = user.ID
		exist_pixel.CityID = user.CityID
		exist_pixel.ClanID = user.ClanID
		if(pixel.CityID == 0){
			exist_pixel.IsCityEdge = false;
			exist_pixel.IsCityInner = false;
		}
		db.Save(&exist_pixel)
		pixel = exist_pixel
	}

	if(!cityCreateMode) {
		available_action.Decrement()
		db.Save(&available_action)

		user.AddLog(db, "add_pixel", pixel)

		if(user.CityID != 0){
			pixel.CheckCityEdge(db)
			square_neighbors := pixel.GetNeighborsBigSquare(db, pixel.CityID)
			for index, neighbor := range square_neighbors {
				_ = index
				neighbor.CheckCityEdge(db)
			}

			user_city := City{}
			db.First(&user_city, user.CityID)

			if(previous_pixel_city_id != pixel.CityID && previous_pixel_city_id != 0) {
				square_neighbors = pixel.GetNeighborsBigSquare(db, previous_pixel_city_id)
				for index, neighbor := range square_neighbors {
					//log.Println(helpers.StructToJson(neighbor.X) + ", " + helpers.StructToJson(neighbor.Y))
					_ = index
					neighbor.CheckCityEdge(db)
				}

				previous_city := City{}
				db.First(&previous_city, previous_pixel_city_id)
				previous_city.CachePixelsCount(db, user)
			}

			user_city.CachePixelsCount(db, user)
		}

		user.CachePixelsCount(db)

		if(user.ClanID != 0){
			if(user.Clan == nil){
				user.Clan = &Clan{}
				db.Model(&user).Related(&user.Clan)
			}
			user.Clan.CachePixelsCount(db, user)
		}

		if(user.WorldID != 0){
			if(user.World == nil){
				user.World = &World{}
				db.Model(&user).Related(&user.World)
			}
			user.World.CachePixelsCount(db, user)
		}
	}

	var created_pixel Pixel
	db.Preload("Type").Find(&created_pixel, pixel.ID)
	pixel_room.AddPixel(int(user.ID), created_pixel)

	if(!cityCreateMode) {
		var user_experience UserProgress
		db.First(&user_experience, "user_id = ? AND type = ?", user.ID, "experience")
		user_experience.Increment(db, "add_pixel")
	}

	return created_pixel
}

func (user *User) CanAddPixel (db *gorm.DB, x int, y int, typeID uint) bool {
	if(x < 0 || y < 0) {
		return false
	}

	var world = World{}
	db.Model(&user).Related(&world)

	if(x > world.MaxX || y > world.MaxY) {
		return false
	}

	var exist_pixel = Pixel{}
	db.Where("x = ?", x).Where("y = ?", y).Where("world_id = ?", user.WorldID).First(&exist_pixel)

	var notInCity = exist_pixel.ID == 0 || exist_pixel.CityID == 0
	var sameCity = exist_pixel.CityID == user.CityID
	var notAPartOfTheCity = !exist_pixel.IsCityInner && !exist_pixel.IsCityEdge

	var simpleCheck = notInCity || sameCity || notAPartOfTheCity;

	if(simpleCheck){
		return simpleCheck
	}

	var neighborsCrossInUserCity = exist_pixel.GetNeighborsCross(db, user.CityID, true)
	var neighborsBigSquareInUserCity = exist_pixel.GetNeighborsBigSquare(db, user.CityID, true)

	var onEdgeOfCities = len(neighborsCrossInUserCity) > 0 && len(neighborsBigSquareInUserCity) >= 10

	return onEdgeOfCities
}

func (user *User) AddRole (db *gorm.DB, role *Role) {
	var user_role = UserRole{
		RoleID: role.ID,
		UserID: user.ID,
		WorldID: user.WorldID,
		CityID: user.CityID,
		ClanID: user.ClanID,
	}
	db.Create(&user_role)
}

func (user *User) HasRole (db *gorm.DB, role_name string, level string, object_id uint) bool {
	query := db.Where("level = ?", level).Where("`key` = ?", role_name)

	if(level == "city"){
		query = query.Where("city_id = ?", object_id)
	} else if(level == "clan"){
		query = query.Where("clan_id = ?", object_id)
	} else if(level == "world"){
		query = query.Where("world_id = ?", object_id)
	} else {
		panic("unexpected:level")
	}

	role := Role{}
	query.First(&role)

	if(role.ID == 0){
		panic("not_exist:role")
	}

	user_role := UserRole{}
	db.Where("user_id = ?", user.ID).Where("role_id = ?", role.ID).First(&user_role)

	return user_role.ID != 0
}

func (user *User) SetOnline (db *gorm.DB, isOnline bool) {
	actual_user := User{}
	db.First(&actual_user, user.ID)
	actual_user.IsOnline = isOnline

	if(isOnline){
		actual_user.IsInactive = false
	} else {
		now_time := time.Now()
		actual_user.LastOnlineAt = &now_time
	}
	db.Save(&actual_user)

	if(user.CityID != 0){
		var user_city = City{}
		db.Model(&user).Related(&user_city, "City")

		var users_count int
		db.Model(&user).Where("city_id = ?", user_city.ID).Where("is_online = ?", true).Count(&users_count)
		user_city.OnlineUsersCount = users_count
		db.Save(&user_city)
	}
}

func (user *User) SetCity (db *gorm.DB, cityID uint) {
	user.CityID = cityID
	db.Save(&user)

	if(user.CityID != 0){
		var user_city = City{}
		db.Model(&user).Related(&user_city, "City")

		user_city.CacheUsersCount(db)
	}
}

func (user *User) SetClan (db *gorm.DB, clanID uint) {
	user.ClanID = clanID
	db.Save(&user)

	if(user.ClanID != 0){
		var user_clan = Clan{}
		db.Model(&user).Related(&user_clan, "Clan")

		user_clan.CacheUsersCount(db)
	}
}

func (user *User) ValidateName(db *gorm.DB) (bool, string) {
	if(!helpers.IsValidNameString(user.Name)) {
		return false, "incorrect"
	}

	var exist_user User
	db.Where("world_id = ?", user.WorldID).First(&exist_user, "name = ?", user.Name)

	if(exist_user.ID != 0) {
		return false, "exist"
	}

	return true, ""
}

func (user *User) ValidateEmail(db *gorm.DB) (bool, string) {
	if(!govalidator.IsEmail(user.Email)) {
		return false, "incorrect"
	}

	var exist_user User
	db.Where("world_id = ?", user.WorldID).First(&exist_user, "email = ?", user.Email)

	if(exist_user.ID != 0) {
		return false, "exist"
	}

	return true, ""
}

func (user *User) AddLog(db *gorm.DB, type_str string, object interface{}){
	var log = UserLog{
		ActionType: type_str,
		UserID: user.ID,
		CityID: user.CityID,
		ClanID: user.ClanID,
		WorldID: user.WorldID,
		ActionData: helpers.StructToJson(object),
	}

	if(reflect.TypeOf(object).Name() == "Pixel"){
		pixel := object.(Pixel)
		log.X = pixel.X
		log.Y = pixel.Y

		db.Model(&pixel).Association("UserLogs").Append(&log)
	}

	if(reflect.TypeOf(object).Name() == "City"){
		city := object.(City)
		log.X = city.X
		log.Y = city.Y

		db.Model(&city).Association("UserLogs").Append(&log)
	}
}

func (user *User) CachePixelsCount(db *gorm.DB){
	db.Model(Pixel{}).Where("user_id = ?", user.ID).Count(&user.PixelsCount)
	db.Model(UserLog{}).Where("action_type = ?", "add_pixel").Where("user_id = ?", user.ID).Count(&user.ManualPixelsCount)
	db.Model(&user).Updates(World{PixelsCount: user.PixelsCount, ManualPixelsCount: user.ManualPixelsCount})
}

func (user *User) InitRelatedObjects(db *gorm.DB)  {
	if(user.ReferID != 0){
		refer_pixels := AvailableUserAction{}
		db.First(&refer_pixels, "user_id = ? AND action_type = ?", user.ReferID, "add_pixel")
		refer_pixels.Value += 10
		db.Save(&refer_pixels)
		pixel_room.ChangeAvailableAction(int(refer_pixels.UserID), refer_pixels)
	}

	var global_chat = Chat{}
	db.Where("type = ?", "global").First(&global_chat, "world_id = ?", user.WorldID)

	var world = World{}
	db.Model(&user).Related(&world)

	var global_chat_member = ChatMember{
		UserID: user.ID,
		ChatID: global_chat.ID,
	}
	db.Create(&global_chat_member)

	var user_first_pixels = AvailableUserAction{
		UserID: user.ID,
		ActionType: "add_pixel",
		Value: 24,
		MaxValue: 24,
		IncrementValue: 1,
		IsAuto: true,
		SecondsPeriod: float32(world.GetSettingIntegerValue(db, "pixel_add_period")),
	}
	db.Create(&user_first_pixels)

	var user_available_clan = AvailableUserAction{
		UserID: user.ID,
		ActionType: "add_clan",
		Value: 1,
		MaxValue: 1,
		IncrementValue: 1,
		IsAuto: false,
	}
	db.Create(&user_available_clan)

	var user_available_city = AvailableUserAction{
		UserID: user.ID,
		ActionType: "add_city",
		Value: 1,
		MaxValue: 1,
		IncrementValue: 1,
		IsAuto: false,
	}
	db.Create(&user_available_city)

	var user_available_join_city = AvailableUserAction{
		UserID: user.ID,
		ActionType: "join_city",
		Value: 1,
		MaxValue: 1,
		IncrementValue: 1,
		IsAuto: true,
		SecondsPeriod: float32(world.GetSettingIntegerValue(db, "city_join_period")),
	}
	db.Create(&user_available_join_city)

	var user_experience = UserProgress{
		UserID: user.ID,
		Type: "experience",
		ParentType: "level",
		Value: 0,
		MaxValue: 20,
		MultipleMaxValue: 1.5,
	}
	db.Create(&user_experience)

	var user_level = UserProgress{
		UserID: user.ID,
		Type: "level",
		Value: 1,
		MaxValue: float32(world.GetSettingIntegerValue(db, "user_max_level")),
	}
	db.Create(&user_level)

	if(user.IsAdmin){
		var super_admin = Role{}
		db.Where("level = ? AND `key` = ? AND world_id = ?", "world", "super_admin", user.WorldID).First(&super_admin)

		user.AddRole(db, &super_admin)
	}
}

type UserSocial struct {
	gorm.Model

	Nickname        string

	Provider     	string
	ProviderID   	string

	Token        	string

	Email        	string

	Avatar 			string
	SmallAvatar 	string

	User         	*User            		`gorm:"ForeignKey:UserID;save_associations:false"`
	UserID       	uint                	`gorm:"index"`
}

type Wallet struct {
	gorm.Model

	User   			*User    				`gorm:"ForeignKey:UserID;save_associations:false"`
	UserID 			uint     				`gorm:"index"`

	Currency		string
	Address			string
	Service			string

	Value			float32
}

type Permission struct {
	gorm.Model

	Key				string
	Type			string
	Level			string
}

type PermissionRole struct {
	gorm.Model

	Permission   	*Permission    			`gorm:"ForeignKey:PermissionID;save_associations:false"`
	PermissionID 	uint     				`gorm:"index"`

	Role       		*Role   				`gorm:"ForeignKey:RoleID;save_associations:false"`
	RoleID     		uint					`gorm:"index"`
}

type Role struct {
	gorm.Model

	Name			string
	Key				string
	Level			string

	IsAdmin			bool

	Owner   		*User    				`gorm:"ForeignKey:OwnerID;save_associations:false"`
	OwnerID 		uint     				`gorm:"index"`

	Clan       		*Clan   				`gorm:"ForeignKey:ClanID;save_associations:false"`
	ClanID     		uint					`gorm:"index"`

	City       		*City   				`gorm:"ForeignKey:CityID;save_associations:false"`
	CityID     		uint					`gorm:"index"`

	World       	*World   				`gorm:"ForeignKey:WorldID;save_associations:false"`
	WorldID     	uint					`gorm:"index"`
}

type UserRole struct {
	gorm.Model

	User   			*User    				`gorm:"ForeignKey:UserID;save_associations:false"`
	UserID 			uint     				`gorm:"index"`

	Role   			*Role    				`gorm:"ForeignKey:RoleID;save_associations:false"`
	RoleID 			uint     				`gorm:"index"`

	Clan       		*Clan   				`gorm:"ForeignKey:ClanID;save_associations:false"`
	ClanID     		uint					`gorm:"index"`

	Chat       		*Chat   				`gorm:"ForeignKey:ChatID;save_associations:false"`
	ChatID     		uint					`gorm:"index"`

	City       		*City   				`gorm:"ForeignKey:CityID;save_associations:false"`
	CityID     		uint					`gorm:"index"`

	World       	*World   				`gorm:"ForeignKey:WorldID;save_associations:false"`
	WorldID     	uint					`gorm:"index"`

	AssignedBy   	*User    				`gorm:"ForeignKey:InvitedByID;save_associations:false"`
	AssignedByID 	uint     				`gorm:"index"`
}

type UserLog struct {
	gorm.Model
	//https://github.com/jinzhu/gorm/blob/master/polymorphic_test.go
	ObjectType		string
	ObjectID		uint

	X 				int
	Y 				int

	ActionType		string

	ActionData      string  				`gorm:"size:2000";sql:"json"`

	User   			*User    				`gorm:"ForeignKey:UserID;save_associations:false"`
	UserID 			uint     				`gorm:"index"`

	City       		*City   				`gorm:"ForeignKey:CityID;save_associations:false"`
	CityID     		uint					`gorm:"index"`

	Clan       		*Clan   				`gorm:"ForeignKey:ClanID;save_associations:false"`
	ClanID     		uint					`gorm:"index"`

	World       	*World   				`gorm:"ForeignKey:WorldID;save_associations:false"`
	WorldID     	uint					`gorm:"index"`
}