package app_models

import (
	"github.com/jinzhu/gorm"
	"geek-pixel/backend/pixel_room"
)

type UserProgress struct {
	gorm.Model

	Type				string
	ParentType			string

	Value				float32
	MaxValue			float32

	MultipleMaxValue	float32

	User         		*User            	`gorm:"ForeignKey:UserID;save_associations:false"`
	UserID       		uint                `gorm:"index"`
}

func (user_progress *UserProgress) Increment (db *gorm.DB, type_str string)  {
	user := User{}
	db.Preload("World").First(&user, "id = ?", user_progress.UserID)

	setting_name := "progress_" + user_progress.Type + "_" + type_str
	increment_value := float32(user.World.GetSettingIntegerValue(db, setting_name))
	user_progress.Value += increment_value

	user_progress.IncrementParent(db, &user)

	db.Save(&user_progress)

	pixel_room.UpdateProgress(int(user.ID), user_progress)
}

func (user_progress *UserProgress) IncrementParent (db *gorm.DB, user *User)  {
	if(user_progress.ParentType == "" || user_progress.Value < user_progress.MaxValue) {
		return;
	}

	user_progress.Value -= user_progress.MaxValue

	var parent_progress UserProgress
	db.First(&parent_progress, "user_id = ? AND type = ?", user.ID, user_progress.ParentType)

	if(parent_progress.ParentType == "" && parent_progress.Value >= parent_progress.MaxValue) {
		user_progress.Value = user_progress.MaxValue
		return
	}

	if(user_progress.MultipleMaxValue != 0) {
		user_progress.MaxValue = float32(int(user_progress.MaxValue * user_progress.MultipleMaxValue))
	}

	parent_progress.Increment(db, "add_" + user_progress.Type)

	if(user_progress.Value >= user_progress.MaxValue) {
		user_progress.IncrementParent(db, user)
	}
}