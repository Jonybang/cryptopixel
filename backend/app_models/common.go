package app_models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"strconv"
	"geek-pixel/backend/helpers"
	"geek-pixel/backend/pixel_room"
)

func Initialize() *gorm.DB {
	db, err := gorm.Open("mysql", "root:Jb192837@/geek_pixel?collation=utf8_unicode_ci&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}
	//defer db.Close()

	// Migrate the schema
	db.Exec("SET NAMES 'utf8'");

	db.LogMode(true)

	return db
}

type World struct {
	gorm.Model

	Name           		string
	Aka 				string  		`gorm:"size:2000";sql:"json"`

	Type 				string
	Icon 				string
	BackgroundColor 	string
	Language 			string

	MaxX				int
	MaxY				int

	UsersCount      	int
	OnlineUsersCount   	int
	PixelsCount     	int
	ManualPixelsCount   int

	Pixels  			[]Pixel    		`gorm:"ForeignKey:WorldID;save_associations:false"`
	Clans  				[]Clan    		`gorm:"ForeignKey:WorldID;save_associations:false"`
	Cities  			[]City    		`gorm:"ForeignKey:WorldID;save_associations:false"`
	Chats  				[]Chat    		`gorm:"ForeignKey:WorldID;save_associations:false"`
	Settings 			[]Setting 		`gorm:"ForeignKey:WorldID;save_associations:false"`

	UserLogs 			[]UserLog 		`gorm:"polymorphic:Object"`
}

func (world *World) GetSettingValue(db *gorm.DB, key string) string {
	//fmt.Println("World.ID: " + string(world.ID))
	var setting Setting
	db.First(&setting, "`key` = ?", key)
	return setting.Value
}

func (world *World) CachePixelsCount(db *gorm.DB, user *User){
	db.Model(Pixel{}).Where("world_id = ?", world.ID).Count(&world.PixelsCount)
	db.Model(UserLog{}).Where("action_type = ?", "add_pixel").Where("world_id = ?", world.ID).Count(&world.ManualPixelsCount)
	db.Model(&world).Updates(World{PixelsCount: world.PixelsCount, ManualPixelsCount: world.ManualPixelsCount})
}

func (world *World) GetSettingIntegerValue(db *gorm.DB, key string) int {
	//TODO: not null word
	var str_value = world.GetSettingValue(db, key)
	i, err := strconv.Atoi(str_value)
	if(err != nil){
		panic(err)
	}
	return i
}

func (world *World) ValidateName(db *gorm.DB) (bool, string) {
	if(!helpers.IsValidNameString(world.Name)) {
		return false, "incorrect"
	}

	var exist_world World
	db.First(&exist_world, "name = ?", world.Name)

	if(exist_world.ID != 0) {
		return false, "exist"
	}

	return true, ""
}

type Clan struct {
	gorm.Model

	Name   				string
	Aka 				string  		`gorm:"size:2000";sql:"json"`

	UsersCount      	int
	OnlineUsersCount   	int
	PixelsCount     	int
	ManualPixelsCount    int

	Users  				[]User    		`gorm:"ForeignKey:ClanID;save_associations:false"`
	Cities 				[]City    		`gorm:"ForeignKey:ClanID;save_associations:false"`

	World          		*World    		`gorm:"ForeignKey:WorldID;save_associations:false"`
	WorldID        		uint     		`gorm:"index"`

	UserLogs 			[]UserLog 		`gorm:"polymorphic:Object"`
}

func (clan *Clan) CachePixelsCount(db *gorm.DB, user *User){
	db.Model(Pixel{}).Where("clan_id = ?", clan.ID).Count(&clan.PixelsCount)
	db.Model(UserLog{}).Where("action_type = ?", "add_pixel").Where("clan_id = ?", clan.ID).Count(&clan.ManualPixelsCount)
	db.Model(&clan).Updates(World{PixelsCount: clan.PixelsCount, ManualPixelsCount: clan.ManualPixelsCount})
}

func (clan *Clan) CacheUsersCount(db *gorm.DB){
	var online_users_count int
	db.Model(User{}).Where("clan_id = ?", clan.ID).Where("is_online = ?", true).Count(&online_users_count)
	clan.OnlineUsersCount = online_users_count

	var users_count int
	db.Model(User{}).Where("clan_id = ?", clan.ID).Count(&users_count)
	clan.UsersCount = users_count

	db.Save(&clan)

	pixel_room.ChangeClan(0, clan)
}

func (clan *Clan) ValidateName(db *gorm.DB) (bool, string) {
	if(!helpers.IsValidNameString(clan.Name)) {
		return false, "incorrect"
	}

	var exist_clan Clan
	db.First(&exist_clan, "name = ? AND world_id = ?", clan.Name, clan.WorldID)

	if(exist_clan.ID != 0) {
		return false, "exist"
	}

	return true, ""
}

type NewsFeed struct {
	gorm.Model

	Text 			string
	Attachments 	string  			`gorm:"size:2000";sql:"json"`

	Creator 		*User    			`gorm:"ForeignKey:CreatorID;save_associations:false"`
	CreatorID 		uint     			`gorm:"index"`

	City   			*City    			`gorm:"ForeignKey:CityID;save_associations:false"`
	CityID 			uint     			`gorm:"index"`

	Clan   			*Clan    			`gorm:"ForeignKey:ClanID;save_associations:false"`
	ClanID 			uint     			`gorm:"index"`

	World          	*World    			`gorm:"ForeignKey:WorldID;save_associations:false"`
	WorldID        	uint     			`gorm:"index"`

	ByAdmin			bool

	UserLogs 		[]UserLog 			`gorm:"polymorphic:Object"`
}

type Setting struct {
	gorm.Model

	Name   			string
	Key   			string
	Value   		string
	Level   		string

	IsEditable		bool
	IsAdmin			bool

	World          	*World    			`gorm:"ForeignKey:WorldID;save_associations:false"`
	WorldID        	uint     			`gorm:"index"`

	City   			*City    			`gorm:"ForeignKey:CityID;save_associations:false"`
	CityID 			uint     			`gorm:"index"`

	Clan   			*Clan    			`gorm:"ForeignKey:ClanID;save_associations:false"`
	ClanID 			uint     			`gorm:"index"`
}