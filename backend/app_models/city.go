package app_models

import (
	"geek-pixel/backend/pixel_room"
	"github.com/jinzhu/gorm"
	"geek-pixel/backend/helpers"
)

type City struct {
	gorm.Model

	Y      				int
	X      				int

	UsersCount      	int
	OnlineUsersCount   	int
	PixelsCount      	int
	ManualPixelsCount    int

	Name           		string

	Aka 				string  		`gorm:"size:2000";sql:"json"`

	IsCreated    		bool
	IsDestroyed    		bool

	Owner 				*User    		`gorm:"ForeignKey:OwnerID;save_associations:false"`
	OwnerID 			uint     		`gorm:"index"`

	Clan          		*Clan    		`gorm:"ForeignKey:ClanID;save_associations:false"`
	ClanID        		uint     		`gorm:"index"`

	World          		*World    		`gorm:"ForeignKey:WorldID;save_associations:false"`
	WorldID        		uint     		`gorm:"index"`

	Pixels  			[]Pixel    		`gorm:"ForeignKey:CityID;save_associations:false"`

	UserLogs 			[]UserLog 		`gorm:"polymorphic:Object"`
}

func (city *City) CreateChat(db *gorm.DB, owner *User) Chat {
	var city_chat = Chat{
		Name: city.Name,
		Type: "city",
		WorldID: city.WorldID,
		CityID: city.ID,
		OwnerID: owner.ID,
	}
	db.Create(&city_chat)

	var city_chat_member = &ChatMember{
		UserID: owner.ID,
		ChatID: city_chat.ID,
	}
	db.Create(&city_chat_member)

	pixel_room.JoinChat(int(owner.ID), city_chat_member)
	pixel_room.JoinCity(int(owner.ID), owner)

	return city_chat
}

func (city *City) JoinUser (db *gorm.DB, user *User) {
	var available_action AvailableUserAction
	db.Where("user_id = ?", user.ID).Where("action_type = ?", "join_city").First(&available_action)

	if(available_action.Value < 1){
		panic("not_available")
	}

	user.SetCity(db, city.ID)
	user.SetClan(db, city.ClanID)

	var city_chat = &Chat{}
	db.Where("type = ?", "city").First(&city_chat, "city_id = ?", city.ID)

	var city_chat_member = &ChatMember{
		UserID: user.ID,
		ChatID: city_chat.ID,
	}
	db.Create(&city_chat_member)

	pixel_room.JoinChat(int(user.ID), city_chat_member)
	pixel_room.JoinCity(int(user.ID), user)

	available_action.Decrement()
	db.Save(&available_action)
}

func (city *City) LeaveUser(db *gorm.DB, user *User)  {
	db.Model(User{}).Where("id = ?", user.ID).Updates(map[string]interface{}{"city_id": 0, "clan_id": 0})

	city.CacheUsersCount(db)

	result_user := User{}
	db.First(&result_user, "id = ?", user.ID)
	pixel_room.LeaveCity(int(user.ID), result_user)
}

func (city *City) CachePixelsCount(db *gorm.DB, user *User){
	db.Model(Pixel{}).Where("city_id = ?", city.ID).Where("(is_city_edge = ?", true).Or("is_city_inner = ?)", true).Count(&city.PixelsCount)
	db.Model(UserLog{}).Where("action_type = ?", "add_pixel").Where("city_id = ?", city.ID).Count(&city.ManualPixelsCount)

	city.IsDestroyed = city.PixelsCount == 0
	db.Model(&city).Updates(map[string]interface{}{"pixels_count": city.PixelsCount, "manual_pixels_count": city.ManualPixelsCount, "is_destroyed": city.IsDestroyed})

	pixel_room.ChangeCity(int(user.ID), city)
}

func (city *City) CacheUsersCount(db *gorm.DB){
	var online_users_count int
	db.Model(User{}).Where("city_id = ?", city.ID).Where("is_online = ?", true).Count(&online_users_count)
	city.OnlineUsersCount = online_users_count

	var users_count int
	db.Model(User{}).Where("city_id = ?", city.ID).Count(&users_count)
	city.UsersCount = users_count

	db.Save(&city)

	pixel_room.ChangeCity(0, city)
}

func (city *City) ValidateName(db *gorm.DB) (bool, string) {
	if(!helpers.IsValidNameString(city.Name)) {
		return false, "incorrect"
	}

	var exist_city City
	db.First(&exist_city, "name = ? AND world_id = ?", city.Name, city.WorldID)

	if(exist_city.ID != 0) {
		return false, "exist"
	}

	return true, ""
}