package app_models

import "github.com/jinzhu/gorm"

type Chat struct {
	gorm.Model

	Name 			string
	Type 			string

	Owner 			*User    			`gorm:"ForeignKey:OwnerID;save_associations:false"`
	OwnerID 		uint     			`gorm:"index"`

	City   			*City    			`gorm:"ForeignKey:CityID;save_associations:false"`
	CityID 			uint     			`gorm:"index"`

	Clan   			*Clan    			`gorm:"ForeignKey:ClanID;save_associations:false"`
	ClanID 			uint     			`gorm:"index"`

	World   		*World    			`gorm:"ForeignKey:WorldID;save_associations:false"`
	WorldID 		uint     			`gorm:"index"`

	Members  		[]ChatMember    	`gorm:"ForeignKey:ChatID;save_associations:false"`
	Messages  		[]ChatMessage    	`gorm:"ForeignKey:ChatID;save_associations:false"`

	UserLogs 		[]UserLog 			`gorm:"polymorphic:Object"`
}

type ChatMember struct {
	gorm.Model

	User   			*User    			`gorm:"ForeignKey:UserID;save_associations:false"`
	UserID 			uint     			`gorm:"index"`

	Chat   			*Chat    			`gorm:"ForeignKey:ChatID;save_associations:false"`
	ChatID 			uint     			`gorm:"index"`

	InvitedBy   	*User    			`gorm:"ForeignKey:InvitedByID;save_associations:false"`
	InvitedByID 	uint     			`gorm:"index"`
}

type ChatMessage struct {
	gorm.Model

	Text 			string

	Chat   			*Chat    			`gorm:"ForeignKey:ChatID;save_associations:false"`
	ChatID 			uint     			`gorm:"index"`

	User   			*User    			`gorm:"ForeignKey:UserID;save_associations:false"`
	UserID 			uint     			`gorm:"index"`

	Attachments 	string  			`gorm:"size:2000";sql:"json"`
}