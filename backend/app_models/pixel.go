package app_models

import (
	"github.com/jinzhu/gorm"
	"time"
	"geek-pixel/backend/pixel_room"
)

type Pixel struct {
	gorm.Model

	Y      			int
	X      			int

	IsCityCorner    bool
	IsCityEdge      bool
	IsCityInner		bool

	Type   			*PixelType    		`gorm:"ForeignKey:TypeID;save_associations:false"`
	TypeID 			uint     			`gorm:"index"`

	User   			*User    			`gorm:"ForeignKey:UserID;save_associations:false"`
	UserID 			uint     			`gorm:"index"`

	City   			*City    			`gorm:"ForeignKey:CityID;save_associations:false"`
	CityID 			uint     			`gorm:"index"`

	Clan          	*Clan    			`gorm:"ForeignKey:ClanID;save_associations:false"`
	ClanID        	uint     			`gorm:"index"`

	World          	*World    			`gorm:"ForeignKey:WorldID;save_associations:false"`
	WorldID        	uint     			`gorm:"index"`

	Effects  		[]PixelEffect    	`gorm:"ForeignKey:PixelID;save_associations:false"`

	UserLogs 		[]UserLog 			`gorm:"polymorphic:Object"`
}

func (pixel *Pixel) GetNeighborsSquare(db *gorm.DB, cityID uint, conditions ...bool) []Pixel {

	var strictMode = false
	if (len(conditions) > 0) {
		strictMode = conditions[0]
	}

	var pixel_size = pixel.World.GetSettingIntegerValue(db, "pixel_size")
	var square_pixels = []Pixel{}
	query := db.Where("city_id = ?", cityID)

	if(strictMode){
		query = query.Where("(is_city_edge = ? OR is_city_inner = ?)", true, true)
	}

	query = query.Where("(X = ? AND Y = ?", pixel.X + pixel_size, pixel.Y)			//right
	query = query.Or("X = ? AND Y = ?", pixel.X + pixel_size, pixel.Y + pixel_size)	//rightBottom
	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size, pixel.Y)				//left
	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size, pixel.Y - pixel_size)	//leftTop
	query = query.Or("X = ? AND Y = ?", pixel.X, pixel.Y + pixel_size)				//bottom
	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size, pixel.Y + pixel_size)	//leftBottom
	query = query.Or("X = ? AND Y = ?", pixel.X, pixel.Y - pixel_size)				//top
	query = query.Or("X = ? AND Y = ?)", pixel.X + pixel_size, pixel.Y - pixel_size)//rightTop

	query.Find(&square_pixels)
	return square_pixels
}

func (pixel *Pixel) GetNeighborsBigSquare(db *gorm.DB, cityID uint, conditions ...bool) []Pixel {

	var strictMode = false
	if (len(conditions) > 0) {
		strictMode = conditions[0]
	}

	var pixel_size = pixel.World.GetSettingIntegerValue(db, "pixel_size")
	var square_pixels = []Pixel{}
	query := db.Where("city_id = ?", cityID)

	if(strictMode){
		query = query.Where("(is_city_edge = ? OR is_city_inner = ?)", true, true)
	}

	query = query.Where("(X = ? AND Y = ?", pixel.X + pixel_size, pixel.Y)				//right
	query = query.Or("X = ? AND Y = ?", pixel.X + pixel_size*2, pixel.Y)				//right*2

	query = query.Or("X = ? AND Y = ?", pixel.X + pixel_size, pixel.Y + pixel_size)		//rightBottom
	query = query.Or("X = ? AND Y = ?", pixel.X + pixel_size, pixel.Y + pixel_size*2)	//rightBottom*2
	query = query.Or("X = ? AND Y = ?", pixel.X + pixel_size*2, pixel.Y + pixel_size)	//right*2Bottom
	query = query.Or("X = ? AND Y = ?", pixel.X + pixel_size*2, pixel.Y + pixel_size*2)	//right*2Bottom*2

	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size, pixel.Y)					//left
	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size*2, pixel.Y)				//left*2

	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size, pixel.Y - pixel_size)		//leftTop
	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size, pixel.Y - pixel_size*2) 	//leftTop*2
	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size*2, pixel.Y - pixel_size) 	//left*2Top
	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size*2, pixel.Y - pixel_size*2) //left*2Top*2

	query = query.Or("X = ? AND Y = ?", pixel.X, pixel.Y + pixel_size)					//bottom
	query = query.Or("X = ? AND Y = ?", pixel.X, pixel.Y + pixel_size*2)				//bottom*2

	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size, pixel.Y + pixel_size)		//leftBottom
	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size, pixel.Y + pixel_size*2) 	//leftBottom*2
	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size*2, pixel.Y + pixel_size) 	//left*2Bottom
	query = query.Or("X = ? AND Y = ?", pixel.X - pixel_size*2, pixel.Y + pixel_size*2) //left*2Bottom*2

	query = query.Or("X = ? AND Y = ?", pixel.X, pixel.Y - pixel_size)					//top
	query = query.Or("X = ? AND Y = ?", pixel.X, pixel.Y - pixel_size*2)				//top*2

	query = query.Or("X = ? AND Y = ?", pixel.X + pixel_size, pixel.Y - pixel_size)		//rightTop
	query = query.Or("X = ? AND Y = ?", pixel.X + pixel_size, pixel.Y - pixel_size*2)	//rightTop*2
	query = query.Or("X = ? AND Y = ?", pixel.X + pixel_size*2, pixel.Y - pixel_size)	//right*2Top
	query = query.Or("X = ? AND Y = ?)", pixel.X + pixel_size*2, pixel.Y - pixel_size*2)//right*2Top*2

	query.Find(&square_pixels)
	return square_pixels
}

func (pixel *Pixel) GetNeighborsCross(db *gorm.DB, cityID uint, conditions ...bool) []Pixel {

	var strictMode = false
	if (len(conditions) > 0) {
		strictMode = conditions[0]
	}

	var pixel_size = pixel.World.GetSettingIntegerValue(db, "pixel_size")
	var cross_pixels = []Pixel{}

	query := db.Where("city_id = ?", cityID)

	if(strictMode){
		query = query.Where("(is_city_edge = ? OR is_city_inner = ?)", true, true)
	}

	query = query.Where("(X = ? AND Y = ?", pixel.X + pixel_size, pixel.Y).Or("X = ? AND Y = ?", pixel.X - pixel_size, pixel.Y).Or("X = ? AND Y = ?", pixel.X, pixel.Y + pixel_size).Or("X = ? AND Y = ?)", pixel.X, pixel.Y - pixel_size).Find(&cross_pixels)
	return cross_pixels
}

func (pixel *Pixel) CheckCityEdge (db *gorm.DB) {
	previous_IsCityEdge := pixel.IsCityEdge
	previous_IsCityInner := pixel.IsCityInner

	strict_square_pixels := pixel.GetNeighborsSquare(db, pixel.CityID, true)
	square_pixels := pixel.GetNeighborsSquare(db, pixel.CityID)
	cross_pixels := pixel.GetNeighborsCross(db, pixel.CityID)

	pixel.IsCityCorner = len(strict_square_pixels) <= 3 && len(cross_pixels) <= 2
	pixel.IsCityEdge = len(square_pixels) >= 3 && len(cross_pixels) < 4
	pixel.IsCityInner = len(square_pixels) >= 6 && len(cross_pixels) == 4

	db.Model(&pixel).Updates(map[string]interface{}{"is_city_corner": pixel.IsCityCorner, "is_city_edge": pixel.IsCityEdge, "is_city_inner": pixel.IsCityInner})

	if(previous_IsCityEdge != pixel.IsCityEdge || previous_IsCityInner != pixel.IsCityInner){
		var edited_pixel Pixel
		db.Preload("Type").Find(&edited_pixel, pixel.ID)
		pixel_room.AddPixel(int(pixel.UserID), edited_pixel)
	}
}

type PixelType struct {
	gorm.Model

	Name  			string
	Key  			string
	Color 			string
	Modifiers 		string  			`gorm:"size:2000";sql:"json"`
}

type PixelEffect struct {
	gorm.Model

	Pixel 			*Pixel    			`gorm:"ForeignKey:PixelID;save_associations:false"`
	PixelID 		uint     			`gorm:"index"`

	EffectType   	*PixelEffectType    `gorm:"ForeignKey:TypeID;save_associations:false"`
	EffectTypeID 	uint     			`gorm:"index"`

	User   			*User    			`gorm:"ForeignKey:UserID;save_associations:false"`
	UserID 			uint     			`gorm:"index"`

	Value 			string

	EndOn			time.Time

	IsDisabled		bool

	UserLogs 		[]UserLog 			`gorm:"polymorphic:Object"`
}

type PixelEffectType struct {
	gorm.Model

	Name  			string
	Color 			string
	Opacity 		float32
}