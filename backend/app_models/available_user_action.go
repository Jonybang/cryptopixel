package app_models

import (
	"time"
	"github.com/jinzhu/gorm"
)

type AvailableUserAction struct {
	gorm.Model

	User   			*User    				`gorm:"ForeignKey:UserID;save_associations:false"`
	UserID 			uint     				`gorm:"index"`

	City       		*City   				`gorm:"ForeignKey:CityID;save_associations:false"`
	CityID     		uint					`gorm:"index"`

	Clan       		*Clan   				`gorm:"ForeignKey:ClanID;save_associations:false"`
	ClanID     		uint					`gorm:"index"`

	Chat       		*Chat   				`gorm:"ForeignKey:ChatID;save_associations:false"`
	ChatID     		uint					`gorm:"index"`

	ActionType		string
	Value			float32
	MaxValue		float32
	IncrementValue	float32
	IsAuto			bool

	SecondsPeriod	float32

	AvailableOn		*time.Time

	Data 			string  				`gorm:"size:2000";sql:"json"`
	Condition 		string  				`gorm:"size:2000";sql:"json"`
}

func (available_action *AvailableUserAction) Decrement()  {
	available_action.Value -= 1;

	if(available_action.IsAuto && available_action.MaxValue > available_action.Value && available_action.AvailableOn == nil) {
		available_on := time.Now().Add(time.Second * time.Duration(available_action.SecondsPeriod))
		available_action.AvailableOn = &available_on
	}
}