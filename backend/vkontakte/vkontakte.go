package vkontakte

import (
	"encoding/json"
	"strconv"
	"strings"
	"github.com/yanple/vk_api"
	"net/http"
	"github.com/jinzhu/gorm"
	"geek-pixel/backend/session"
	"geek-pixel/backend/app_models"
	"geek-pixel/backend/helpers"
)

type Vkontakte struct {
	appId string
	appSecret string
	redirectUrl string
	scopes string

	Api vk_api.Api
	request *http.Request
	session session.AppSession
}

func NewVkontakte(session session.AppSession) *Vkontakte {
	app_config := helpers.GetConfig()

	var vk = &Vkontakte{
		appId: "6241941",
		appSecret: "2fUpL9wKTorp4GX1YnQG",
		redirectUrl: app_config.BaseURL + "/vkontakte/token",
		//scopes: "friends,groups",
		request: session.Request,
		session: session,
	}

	if(session.Get("vk_user_token") != nil){
		vk.Api.AccessToken = session.Get("vk_user_token").(string)
		//log.Println("[LOG] get from session vk_user_token", vk.Api.AccessToken)
	}

	if(session.Get("vk_user_id") != nil){
		vk.Api.UserId = session.Get("vk_user_id").(int)
	}
	return vk
}

func (v *Vkontakte) GetAuthUrl() string {
	authUrl, err := v.Api.GetAuthUrl(
		v.redirectUrl,
		v.appId,
		v.scopes)

	if err != nil {
		panic(err)
	}

	return authUrl
}

func (v *Vkontakte) Oauth()  {
	code := v.request.URL.Query().Get("code")

	err := v.Api.OAuth(
		v.redirectUrl,
		v.appSecret,
		v.appId,
		code)
	if err != nil {
		panic(err)
	}
}

func (v *Vkontakte) GetUserById(id int) map[string]interface{} {
	params := make(map[string]string)
	params["user_ids"] = strconv.Itoa(id)
	params["fields"] = "photo_50,photo_200"

	res := v.Request("users.get", params)

	if(len(res) < 1) {
		return nil
	}
	return res[0]
}

func (v *Vkontakte) GetCurrentUser() map[string]interface{} {
	if(v.Api.UserId == 0){
		return nil
	}
	return v.GetUserById(v.Api.UserId)
}

func (v *Vkontakte) GetOrCreateDbUserByVkUser(db *gorm.DB, vkUser map[string]interface{}, world_id uint, refer_id uint) *app_models.User {
	vk_user_id := strconv.FormatFloat(vkUser["id"].(float64), 'f', 0, 64)

	var db_user = &app_models.User{}
	db.Where("world_id = ?", world_id).Where("id IN (?)", db.Model(&app_models.UserSocial{}).Where("provider = ? AND provider_id = ?", "vkontakte", vk_user_id).Select("`user_id`").QueryExpr()).First(&db_user)

	if(db_user.ID != 0){
		return db_user
	}

	user_ref_link := helpers.RandomString(10)
	exist_ref_link_user := &app_models.User{}
	db.First(&exist_ref_link_user, "ref_link = ?", user_ref_link)

	for exist_ref_link_user.ID != 0 {
		user_ref_link = helpers.RandomString(10)
		exist_ref_link_user = &app_models.User{}
		db.First(&exist_ref_link_user, "ref_link = ?", user_ref_link)
	}

	db_user = &app_models.User{
		Name:        	vkUser["first_name"].(string) + " " + vkUser["last_name"].(string),
		WorldID: 		world_id,
		IsAdmin:  		vk_user_id == "11204355",
		RefLink:		user_ref_link,
		ReferID:		refer_id,
	}

	db.Create(&db_user)

	user_social := &app_models.UserSocial{
		UserID: 		db_user.ID,
		Nickname: 		db_user.Name,
		Provider:    	"vkontakte",
		ProviderID:  	vk_user_id,
		Token:       	v.Api.AccessToken,
		Avatar:      	vkUser["photo_200"].(string),
		SmallAvatar: 	vkUser["photo_50"].(string),
	}

	db.Create(&user_social)

	db_user.InitRelatedObjects(db)

	return db_user
}

func (v *Vkontakte) GetOwnerIdByLink(url string){
	lastChar  := url[len(url)-3:]
	if(lastChar == "/"){
		url = url[:len(url) - 1]
	}

	split_url := strings.Split(url, "/")

	params := make(map[string]string)
	params["domain"] = split_url[len(split_url) - 1]
	params["count"] = "1"

	//res := v.Request("wall.get", params)
	//provider_id := res[0]["to_id"]

	//log.Println("provider_id", provider_id)
}

func (v *Vkontakte) Request(method string, params map[string]string) []map[string]interface{} {
	params["v"] = "5.71"

	strResp, err := v.Api.Request(method, params)
	if err != nil {
		panic(err)
	}

	//log.Println("[LOG] " + method + " response ->", strResp)

	var res map[string][]map[string]interface{}
	json.Unmarshal([]byte(strResp), &res)
	return res["response"]
}