package helpers

import (
	"encoding/json"
	"reflect"
	"strings"
	"bytes"
	"io"
	"github.com/gernest/utron/config"
	"unicode"
	"unicode/utf8"
	"math/rand"
)

func StructToJson(v interface{}) string {
	out, err := json.Marshal(v)
	if err != nil {
		panic(err)
	}

	return string(out)
}
//var obj app_models.Pixel
//helpers.JsonToStruct("{"x":400,"y":100,"type_id":1}", &obj)
//var obj map[string]int
//helpers.JsonToStruct("{"x":400,"y":100,"type_id":1}", &obj)
func JsonToStruct(v string, out interface{}) {
	dec := json.NewDecoder(strings.NewReader(v))
	if err := dec.Decode(&out); err != nil {
		panic(err)
	}
}

func RequestBodyToStruct(body io.ReadCloser, out interface{}) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(body)
	bodyStr := buf.String()
	JsonToStruct(bodyStr, &out)
}

func GetStructValue(s struct{}, field string) int {
	r := reflect.ValueOf(s)
	f := reflect.Indirect(r).FieldByName(field)
	return int(f.Int())
}

func GetConfig() *config.Config {
	app_config, err := config.NewConfig("config/app.json")
	if err != nil {
		panic(err)
	}
	return app_config
}

func IsValidNameString(s string) bool {
	if(s == ""){
		return false
	}

	if(utf8.RuneCountInString(s) > 32) {
		return false
	}

	for _, r := range []rune(s) {
		if (!unicode.Is(unicode.Cyrillic, r) && !unicode.Is(unicode.Latin, r) && !unicode.Is(unicode.Number, r) && !unicode.Is(unicode.Space, r)) {
			return false
		}
	}

	return true
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func RandomJoin(table string) string {
	return "JOIN ( SELECT (SELECT MIN(id) FROM " + table + ") + RAND() * ((SELECT MAX(id) FROM " + table + ") - (SELECT MIN(id) FROM " + table + ")) AS random_id ) AS random"
}

type AreaStruct struct {
	StartX		int
	StartY		int
	EndX		int
	EndY		int
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Int63() % int64(len(letterBytes))]
	}
	return string(b)
}