package seeds

import (
	"github.com/jinzhu/gorm"
	"geek-pixel/backend/app_models"
)

func Run(db *gorm.DB, test_data bool) {
	var worlds []app_models.World
	db.Find(&worlds)
	if(worlds != nil && len(worlds) > 0){
		return
	}

	var en_world = app_models.World{
		Name: "First EN",
		Type: "basic",
		BackgroundColor: "white",
		Language: "en",
		MaxX: 10000,
		MaxY: 10000,
	}
	db.Create(&en_world)

	var ru_world = app_models.World{
		Name: "Первый RU",
		Type: "basic",
		BackgroundColor: "white",
		Language: "ru",
		MaxX: 5000,
		MaxY: 5000,
	}
	db.Create(&ru_world)

	var en_chat = app_models.Chat{
		Name: "Global",
		Type: "global",
		WorldID: en_world.ID,
	}
	db.Create(&en_chat)

	var ru_chat = app_models.Chat{
		Name: "Глобальный",
		Type: "global",
		WorldID: ru_world.ID,
	}
	db.Create(&ru_chat)

	var en_news = app_models.NewsFeed{
		Text: "We started! Welcome everyone!",
		WorldID: en_world.ID,
		ByAdmin: true,
	}
	db.Create(&en_news)

	var ru_news = app_models.NewsFeed{
		Text: "Мы запустились! Добро пожаловать всем!",
		WorldID: ru_world.ID,
		ByAdmin: true,
	}
	db.Create(&ru_news)

	permissions(db)

	pixels(db)

	settings(db)

	if(test_data) {
		var en_user = &app_models.User{
			Name: "Test user",
			WorldID: en_world.ID,
			IsOnline: true,
		}
		db.Create(&en_user)

		en_user.InitRelatedObjects(db)

		en_user.CreateClan(db, "Test clan")
		en_user.CreateCity(db, "Test city", 500, 500)

		var ru_user = &app_models.User{
			Name: "Тестовый юзер",
			WorldID: ru_world.ID,
			IsOnline: true,
		}
		db.Create(&ru_user)

		ru_user.InitRelatedObjects(db)

		ru_user.CreateClan(db, "Тестовый клан")
		ru_user.CreateCity(db, "Тестовый город", 500, 500)
	}
}

func permissions (db *gorm.DB){

	var worlds []app_models.World
	db.Find(&worlds)

	for index, world := range worlds {
		_ = index

		var super_admin = app_models.Role{
			Name: "Super Admin",
			Key: "super_admin",
			Level: "world",
			WorldID: world.ID,
			IsAdmin: true,
		}
		db.Create(&super_admin)

		var permissions_array = [21][2]string{
			[2]string{"financial", "city"},
			[2]string{"financial", "clan"},
			[2]string{"financial", "world"},

			[2]string{"members", "city"},
			[2]string{"members", "clan"},
			[2]string{"members", "world"},

			[2]string{"news", "city"},
			[2]string{"news", "clan"},
			[2]string{"news", "world"},

			[2]string{"chats", "city"},
			[2]string{"chats", "clan"},
			[2]string{"chats", "world"},
		}

		for i := 0; i < len(permissions_array); i++ {
			permission := app_models.Permission{Key: permissions_array[i][0], Type: permissions_array[i][1]}
			db.Create(&permission)

			var permission_role = app_models.PermissionRole{
				PermissionID: permission.ID,
				RoleID: super_admin.ID,
			}
			db.Create(&permission_role)
		}
	}
}

func pixels (db *gorm.DB){
	var pixel_types_array = [21][3]string{
		[3]string{"Pixel 1", "#F44336", ""},
		[3]string{"Pixel 2", "#E91E63", ""},
		[3]string{"Pixel 3", "#9C27B0", ""},
		[3]string{"Pixel 4", "#673AB7", ""},
		[3]string{"Pixel 5", "#3F51B5", ""},
		[3]string{"Pixel 6", "#2196F3", ""},
		[3]string{"Pixel 7", "#03A9F4", ""},
		[3]string{"Pixel 8", "#00BCD4", ""},
		[3]string{"Pixel 9", "#009688", ""},
		[3]string{"Pixel 10", "#4CAF50", ""},
		[3]string{"Pixel 11", "#8BC34A", ""},
		[3]string{"Pixel 12", "#CDDC39", ""},
		[3]string{"Pixel 13", "#FFEB3B", ""},
		[3]string{"Pixel 14", "#FFC107", ""},
		[3]string{"Pixel 15", "#FF9800", ""},
		[3]string{"Pixel 16", "#FF5722", ""},
		[3]string{"Pixel 17", "#795548", ""},
		[3]string{"Pixel 18", "#9E9E9E", "grey"},
		[3]string{"Pixel 19", "#607D8B", ""},
		[3]string{"Pixel 20", "#000000", "black"},
		[3]string{"Pixel 21", "#FFFFFF", "empty"},
	}

	for i := 0; i < len(pixel_types_array); i++ {
		pixel := app_models.PixelType{Name: pixel_types_array[i][0], Color: pixel_types_array[i][1], Key: pixel_types_array[i][2]}
		db.Create(&pixel)
	}
}

func settings (db *gorm.DB){
	var worlds []app_models.World
	db.Find(&worlds)

	for index, world := range worlds {
		_ = index
		var settings_array = [9][4]interface{}{
			[4]interface{}{"Pixel Add Timeout", "pixel_add_period", "300", true}, //TODO: 3600
			[4]interface{}{"City Join Timeout", "city_join_period", "604800", true}, //week
			[4]interface{}{"Pixel Size", "pixel_size", "5", false},
			[4]interface{}{"Base City Radius", "base_city_radius", "25", true},
			[4]interface{}{"User Max Level", "user_max_level", "80", true},
			[4]interface{}{"Experience Per Pixel", "progress_experience_add_pixel", "1", true},
			[4]interface{}{"Level increment", "progress_level_add_experience", "1", true},
			[4]interface{}{"Seconds to inactive user", "inactive_seconds_period", "60", true},// TODO: 259200
			[4]interface{}{"Seconds to inactive user", "inactive_penalty_cron", "@every 1m", true},// TODO: @hourly
		}

		for i := 0; i < len(settings_array); i++ {
			setting := app_models.Setting{Name: settings_array[i][0].(string), Key: settings_array[i][1].(string), Value: settings_array[i][2].(string), IsEditable: settings_array[i][3].(bool), IsAdmin: true, WorldID: world.ID, Level: "world" }
			db.Create(&setting)
		}

		setting := app_models.Setting{Name: "Version", Key: "version", Value: "1", IsEditable: false, IsAdmin: true, Level: "app"}
		db.Create(&setting)
	}
}