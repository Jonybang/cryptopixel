package pixel_room

import (
	"container/list"
	"time"
	"geek-pixel/backend/helpers"
)


type Event struct {
	Type      string // "join", "leave", "add_message", "add_pixel", "add_news"
	User      int
	Timestamp int    // Unix timestamp (secs)
	Data	  string
}

type Subscription struct {
	New     <-chan Event // New events coming in.
}

// Owner of a subscription must cancel it when they stop listening to events.
func (s Subscription) Cancel() {
	unsubscribe <- s.New // Unsubscribe the channel.
	drain(s.New)         // Drain it, just in case there was a pending publish.
}

func newEvent(typ string, user int, msg string) Event {
	return Event{typ, user, int(time.Now().Unix()), msg}
}
func newDataEvent(typ string, user int, data interface{}) Event {
	dataJson := helpers.StructToJson(data)
	return Event{typ, user, int(time.Now().Unix()), dataJson}
}

func Subscribe() Subscription {
	resp := make(chan Subscription)
	subscribe <- resp
	return <-resp
}

func Join(user int) {
	publish <- newEvent("join", user, "")
}

func AddPixel(user int, data interface{}) {
	publish <- newDataEvent("add_pixel", user, data)
}

func AddMessage(user int, data interface{}) {
	publish <- newDataEvent("add_message", user, data)
}

func AddNews(user int, data interface{}) {
	publish <- newDataEvent("add_news", user, data)
}

func ChangeAvailableAction(user int, data interface{})  {
	publish <- newDataEvent("change_available_action", user, data)
}

func AddCity(user int, data interface{})  {
	publish <- newDataEvent("add_city", user, data)
}

func JoinCity(user int, data interface{})  {
	publish <- newDataEvent("join_city", user, data)
}

func LeaveCity(user int, data interface{})  {
	publish <- newDataEvent("leave_city", user, data)
}

func ChangeCity(user int, data interface{})  {
	publish <- newDataEvent("change_city", user, data)
}

func ChangeClan(user int, data interface{})  {
	publish <- newDataEvent("change_clan", user, data)
}

func JoinChat(user int, data interface{})  {
	publish <- newDataEvent("join_chat", user, data)
}

func UpdateProgress(user int, data interface{})  {
	publish <- newDataEvent("update_progress", user, data)
}

func SendError(user int, data interface{})  {
	publish <- newDataEvent("error", user, data)
}

func Leave(user int) {
	publish <- newEvent("leave", user, "")
}

var (
	// Send a channel here to get room events back.  It will send the entire
	// archive initially, and then new messages as they come in.
	subscribe = make(chan (chan<- Subscription), 10)
	// Send a channel here to unsubscribe.
	unsubscribe = make(chan (<-chan Event), 10)
	// Send events here to publish them.
	publish = make(chan Event, 10)
)

// This function loops forever, handling the chat room pubsub
func chatroom() {
	subscribers := list.New()

	for {
		select {
		case ch := <-subscribe:
			//fmt.Println("subscribe")
			subscriber := make(chan Event, 10)
			subscribers.PushBack(subscriber)
			ch <- Subscription{subscriber}

		case event := <-publish:
			//fmt.Println("event publish")
			for ch := subscribers.Front(); ch != nil; ch = ch.Next() {
				ch.Value.(chan Event) <- event
			}

		case unsub := <-unsubscribe:
			for ch := subscribers.Front(); ch != nil; ch = ch.Next() {
				if ch.Value.(chan Event) == unsub {
					subscribers.Remove(ch)
					break
				}
			}
		}
	}
}

func init() {
	go chatroom()
}

// Helpers

// Drains a given channel of any messages.
func drain(ch <-chan Event) {
	for {
		select {
		case _, ok := <-ch:
			if !ok {
				return
			}
		default:
			return
		}
	}
}