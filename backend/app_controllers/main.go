package app_controllers

import (
	"net/http"
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/session"
	"strings"
	"geek-pixel/backend/app_models"
)

type Main struct {
	controller.BaseController
	Routes []string
}

func (c *Main) Home() {
	request := c.Ctx.Request()
	ref := request.URL.Query().Get("ref")

	if(ref != ""){
		ref_user := app_models.User{}
		c.Ctx.DB.DB.Preload("World").First(&ref_user, "ref_link = ?", ref)

		appSession := session.NewAppSession(request, c.Ctx.SessionStore)
		appSession.Set("ref", ref)
		if(ref_user.World != nil){
			appSession.SetCurrentWorld(ref_user.World)
		}
		appSession.Save(c.Ctx.Response())
	}

	c.Ctx.Template = "index"
	if(strings.Contains(request.URL.Path, "ru")){
		c.Ctx.Data["lang"] = "ru"
	} else {
		c.Ctx.Data["lang"] = "en"
	}
	c.HTML(http.StatusOK)
}

func NewMain() controller.Controller {
	return &Main{
		Routes: []string{
			"get;/;Home",
			"get;/ru/;Home",
		},
	}
}