package app_controllers

import (
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"net/http"
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/vkontakte"
	"geek-pixel/backend/session"
	"geek-pixel/backend/app_models"
	"log"
	"geek-pixel/backend/helpers"
)

type Vkontakte struct {
	controller.BaseController
	Routes []string
}

func (c *Vkontakte) Auth() {
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	vk := vkontakte.NewVkontakte(appSession)
	authUrl := vk.GetAuthUrl()

	http.Redirect(c.Ctx.Response(), c.Ctx.Request(), authUrl, http.StatusFound)
}

func (c *Vkontakte) Token() {
	response := c.Ctx.Response()
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	db := c.Ctx.DB.DB

	if(appSession.GetCurrentWorldId() == 0){
		c.Ctx.Data["message"] = "Current world not found"
		c.Ctx.Template = "error"
		c.HTML(http.StatusOK)
		return
	}

	var vk = vkontakte.NewVkontakte(appSession)
	vk.Oauth()

	access_token := vk.Api.AccessToken

	appSession.Set("vk_user_id", vk.Api.UserId)
	appSession.Set("vk_user_token", access_token)
	appSession.Save(response)

	vkUser := vk.GetCurrentUser()
	if(vkUser == nil){
		log.Println("access_token: " + access_token + " UserId: " + helpers.StructToJson(vk.Api.UserId))
		c.Ctx.Data["message"] = "Vkontakte authorization failed"
		c.Ctx.Template = "error"
		c.HTML(http.StatusOK)
		return
	}

	ref := appSession.Get("ref")
	ref_user := app_models.User{}
	if(ref != ""){
		db.First(&ref_user, "ref_link = ?", ref)
	}

	user := vk.GetOrCreateDbUserByVkUser(db, vkUser, appSession.GetCurrentWorldId(), ref_user.ID)

	appSession.SetCurrentUser(user)
	appSession.Save(response)

	c.Ctx.Template = "close"

	c.HTML(http.StatusOK)
}
func NewVkontakte() controller.Controller {
	return &Vkontakte{}
}