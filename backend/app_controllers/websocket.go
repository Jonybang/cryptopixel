package app_controllers

import (
	"log"
	"github.com/gorilla/websocket"
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/pixel_room"
	"geek-pixel/backend/session"
	"geek-pixel/backend/app_models"
	"geek-pixel/backend/helpers"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type Websocket struct {
	controller.BaseController
	Routes []string
}

func (c *Websocket) Socket() {
	req := c.Ctx.Request()
	res := c.Ctx.Response()
	db := c.Ctx.DB.DB

	//event_type := req.URL.Query().Get("type")

	appSession := session.NewAppSession(req, c.Ctx.SessionStore)
	user_id := appSession.GetCurrentUserId()
	world_id := appSession.GetCurrentWorldId()
	if(world_id == 0){
		return
	}

	//fmt.Println("Connection:", req.Header["Connection"])
	conn, err := upgrader.Upgrade(res, req, nil)
	if err != nil {
		log.Println("Upgrade error: ", err)
		return
	}

	var db_user app_models.User

	if(user_id != 0){
		db_user := appSession.GetCurrentUser(db)
		db_user.SetOnline(db, true)
	}

	subscription := pixel_room.Subscribe()
	defer func() {
		subscription.Cancel()
		//log.Println("pixel_room subscription cancel", user_id)

		if(user_id != 0) {
			var user app_models.User
			db.First(&user, user_id)

			if (user.ID != 0) {
				user.SetOnline(db, false)
			}
		}
	}()

	pixel_room.Join(user_id)
	defer func() {
		//log.Println("pixel_room Leave", user_id)

		if(user_id != 0) {
			var user app_models.User
			db.First(&user, user_id)

			if (user.ID != 0) {
				user.SetOnline(db, false)
			}
		}

		pixel_room.Leave(user_id)
	}()

	//log.Println("pixel_room Join")

	// In order to select between websocket messages and subscription events, we
	// need to stuff websocket events into a channel.
	AddEvents := make(chan string)
	go func() {
		for {
			_, b, err := conn.ReadMessage()
			if err != nil {
				close(AddEvents)
				//log.Println("close AddEvents")
				return
			}
			//log.Println("AddEvents")
			AddEvents <- string(b)
		}
	}()

	user_area := helpers.AreaStruct{}

	// Now listen for new events from either the websocket or the chatroom.
	for {
		select {
		case event := <-subscription.New:
			//log.Println("WriteJSON: " + helpers.StructToJson(event))
			//fmt.Println(event.Type)

			if(event.Type == "add_pixel"){
				var event_pixel app_models.Pixel
				helpers.JsonToStruct(event.Data, &event_pixel)

				if(event_pixel.WorldID != world_id){
					//fmt.Println("unsuitable world(" + helpers.StructToJson(event_pixel.WorldID) + ")for user: ", helpers.StructToJson(db_user.ID))
					break
				}

				//fmt.Println("event_pixel: " + helpers.StructToJson(event_pixel))
				//fmt.Println("area: " + helpers.StructToJson(user_area))
				//fmt.Println("event_pixel.X > area.End_x: " + helpers.StructToJson(event_pixel.X > user_area.EndX))

				if(event_pixel.UserID == 0 || int(event_pixel.UserID) != user_id){
					if(event_pixel.X < user_area.StartX || event_pixel.X > user_area.EndX || event_pixel.Y < user_area.StartY || event_pixel.Y > user_area.EndY) {
						//fmt.Println("break")
						break
					}
				}
			} else if(event.Type == "add_message"){
				var event_message app_models.ChatMessage
				helpers.JsonToStruct(event.Data, &event_message)

				var chat app_models.Chat
				db.First(&chat, event_message.ChatID)
				if(chat.Type != "global" && chat.WorldID != world_id){
					if(user_id == 0){
						break
					}
					var chat_member app_models.ChatMember
					db.Where("user_id = ?", db_user.ID).Where("chat_id = ?", event_message.ChatID).First(&chat_member)
					if(chat_member.ID == 0){
						break
					}
				}
			} else if(event.Type == "add_news"){
				var event_news app_models.NewsFeed
				helpers.JsonToStruct(event.Data, &event_news)

				if(event_news.WorldID != world_id){
					break
				}
			} else if(event.Type == "add_city" || event.Type == "change_city"){
				var city app_models.City
				helpers.JsonToStruct(event.Data, &city)

				if(city.WorldID != world_id){
					break
				}
			} else if(event.Type == "join_city"){
				var city_user app_models.User
				helpers.JsonToStruct(event.Data, &city_user)

				if(city_user.WorldID != world_id){
					break
				}
			} else if(event.Type == "join_chat"){
				var chat_member app_models.ChatMember
				helpers.JsonToStruct(event.Data, &chat_member)

				var chat app_models.Chat
				db.First(&chat, chat_member.ChatID)
				if(chat.Type != "global" && chat.WorldID != world_id) {
					if (user_id == 0) {
						break
					}

					var chat_member app_models.ChatMember
					db.Where("user_id = ?", db_user.ID).Where("chat_id = ?", chat_member.ChatID).First(&chat_member)
					if(chat_member.ID == 0){
						break
					}
				}
			} else if(event.Type == "change_available_action" || event.Type == "update_progress" || event.Type == "error"){
				if(user_id == 0 || user_id != event.User){
					break
				}
			}

			//log.Println("WriteJSON")
			if conn.WriteJSON(&event) != nil {
				//log.Println("WriteJSON return")
				return
			}
		case msg, ok := <-AddEvents:
			// If the channel is closed, they disconnected.
			if !ok {
				return
			}
			_ = msg

			//update user area
			helpers.JsonToStruct(msg, &user_area)
			//fmt.Println("Message: ", msg)
		}
	}
	return
}

func NewWebsocket() controller.Controller {
	return &Websocket{
		Routes: []string{
			"get;/websocket/socket;Socket",
		},
	}
}