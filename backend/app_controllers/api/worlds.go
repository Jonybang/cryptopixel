package api

import (
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/app_models"
	"geek-pixel/backend/session"
	"geek-pixel/backend/helpers"
)

type Worlds struct {
	controller.BaseController
	Routes []string
}

func (c *Worlds) GetWorlds()  {
	var worlds_list []app_models.World
	c.Ctx.DB.Order("created_at").Find(&worlds_list)

	c.RenderJSON(worlds_list, 200)
}

func (c *Worlds) GetCurrentWorld()  {
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	world := appSession.GetCurrentWorld(c.Ctx.DB.DB)
	appSession.Save(c.Ctx.Response())

	if(world == nil){
		c.RenderJSON("", 401)
	} else {
		c.RenderJSON(world, 200)
	}
}

func (c *Worlds) GetWorldLogs() {
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)

	if(world_id == 0){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var worldLogs []app_models.UserLog
	c.Ctx.DB.Where("world_id = ?", world_id).Where("world_id = ?", world_id).Preload("User").Preload("City").Preload("Clan").Order("created_at desc").Limit(25).Find(&worldLogs)

	c.RenderJSON(worldLogs, 200)
}

func (c *Worlds) SetWorld()  {
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)

	db := c.Ctx.DB.DB

	var world app_models.World
	db.First(&world, c.Ctx.Params["world_id"])

	if(world.ID == 0){
		c.Ctx.Data["Message"] = "not_found"
		c.RenderJSON(c.Ctx.Data, 500)
		return;
	}
	appSession.SetCurrentWorld(&world)
	appSession.Save(c.Ctx.Response())

	c.RenderJSON(world, 200)
}

func (c *Worlds) GetWorldSettings()  {
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	world := appSession.GetCurrentWorld(c.Ctx.DB.DB)

	if(world == nil){
		c.RenderJSON("", 401)
	} else {
		settings := []app_models.Setting{}
		c.Ctx.DB.DB.Find(&settings, "world_id = ?", world.ID)
		c.RenderJSON(settings, 200)
	}
}

func (c *Worlds) ChangeName(){
	req := c.Ctx.Request()
	db := c.Ctx.DB.DB

	world := &app_models.World{}
	helpers.RequestBodyToStruct(req.Body, &world)

	is_valid, msg := world.ValidateName(db)

	if(!is_valid) {
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = msg
		c.RenderJSON(c.Ctx.Data, 400)
		return;
	}

	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	if(!db_user.HasRole(db, "super_admin", "world", db_user.WorldID)){
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = "not_permitted"
		c.RenderJSON(c.Ctx.Data, 403)
		return;
	}

	db_world := &app_models.World{}
	db.First(&db_world, "id = ?", db_user.WorldID)

	var aka_array []string
	add_to_aka := true

	if(db_world.Aka != ""){
		helpers.JsonToStruct(db_world.Aka, &aka_array)
		add_to_aka = !helpers.StringInSlice(db_world.Name, aka_array)
	}

	if(add_to_aka){
		aka_array = append(aka_array, db_world.Name)
	}

	db_world.Name = world.Name
	db_world.Aka = helpers.StructToJson(aka_array)
	db.Save(&db_world)

	c.RenderJSON(db_world, 200)
}

func (c *Worlds) ChangeSetting(){
	req := c.Ctx.Request()
	db := c.Ctx.DB.DB

	setting := &app_models.Setting{}
	helpers.RequestBodyToStruct(req.Body, &setting)

	db_setting := &app_models.Setting{}
	db.First(&db_setting, "`key` = ? AND world_id = ?", setting.Key, session.GetCurrentWorldIdByContext(c.Ctx))

	if(setting.ID == 0) {
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = "not_found"
		c.RenderJSON(c.Ctx.Data, 400)
		return;
	}

	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	if(!db_user.HasRole(db, "super_admin", "world", db_user.WorldID)){
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = "not_permitted"
		c.RenderJSON(c.Ctx.Data, 403)
		return;
	}

	db_setting.Value = setting.Value
	db.Save(&db_setting)

	c.RenderJSON(db_setting, 200)
}

func (c *Worlds) ChangeProperty(){
	req := c.Ctx.Request()
	db := c.Ctx.DB.DB

	world := &app_models.World{}
	helpers.RequestBodyToStruct(req.Body, &world)

	if(world.MaxX == 0 && world.MaxY == 0) {
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = "invalid"
		c.RenderJSON(c.Ctx.Data, 400)
		return;
	}

	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	if(!db_user.HasRole(db, "super_admin", "world", db_user.WorldID)){
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = "not_permitted"
		c.RenderJSON(c.Ctx.Data, 403)
		return;
	}

	db_world := &app_models.World{}
	db.First(&db_world, "id = ?", db_user.WorldID)

	if(world.MaxX != 0){
		db_world.MaxX = world.MaxX
	}
	if(world.MaxY != 0){
		db_world.MaxY = world.MaxY
	}
	db.Save(&db_world)

	c.RenderJSON(db_world, 200)
}

func NewWorlds() controller.Controller {
	return &Worlds{
		Routes: []string{
			"get;/api/v1/worlds;GetWorlds",
			"post;/api/v1/set_world/{world_id};SetWorld",
			"get;/api/v1/current_world;GetCurrentWorld",
			"get;/api/v1/world_settings;GetWorldSettings",
			"get;/api/v1/world_logs;GetWorldLogs",
			"post;/api/v1/world_change_name;ChangeName",
			"post;/api/v1/world_change_property;ChangeProperty",
			"post;/api/v1/world_change_setting;ChangeSetting",
		},
	}
}