package api

import (
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/session"
	"geek-pixel/backend/helpers"
	"geek-pixel/backend/app_models"
)

type Cities struct {
	controller.BaseController
	Routes []string
}

func (c *Cities) GetCities()  {
	var cities_list []app_models.City
	c.Ctx.DB.Order("created_at").Where("world_id = ?", session.GetCurrentWorldIdByContext(c.Ctx)).Find(&cities_list)

	c.RenderJSON(cities_list, 200)
}

func (c *Cities) GetCurrentCity()  {
	db := c.Ctx.DB.DB
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	user := appSession.GetCurrentUser(c.Ctx.DB.DB)

	if(user == nil){
		c.RenderJSON("", 401)
	} else {
		city := app_models.City{}
		db.Model(&user).Related(&city)
		c.RenderJSON(city, 200)
	}
}

func (c *Cities) CreateCity()  {
	db := c.Ctx.DB.DB
	req := c.Ctx.Request()
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)

	db_user := appSession.GetCurrentUser(db)
	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var city app_models.City
	helpers.RequestBodyToStruct(req.Body, &city)

	if(db_user.ClanID == 0) {
		is_valid, msg := city.Clan.ValidateName(db)

		if(!is_valid) {
			c.Ctx.Data["Valid"] = false
			c.Ctx.Data["Message"] = msg + "_clan"
			c.RenderJSON(c.Ctx.Data, 400)
			return;
		}
	}

	city.WorldID = db_user.WorldID
	is_valid, msg := city.ValidateName(db)

	if(!is_valid) {
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = msg + "_city"
		c.RenderJSON(c.Ctx.Data, 400)
		return;
	}

	defer func(){
		if r := recover(); r != nil {
			c.Ctx.Data["Message"] = r
			c.RenderJSON(c.Ctx.Data, 403)
			return;
		}
	}()

	if(db_user.ClanID == 0) {
		db_user.CreateClan(db, city.Clan.Name)
	}

	db_user.CreateCity(db, city.Name, city.X, city.Y)

	c.RenderJSON("", 200)
}

func (c *Cities) JoinCity()  {
	db := c.Ctx.DB.DB
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)

	db_user := appSession.GetCurrentUser(db)
	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var city app_models.City
	db.First(&city, c.Ctx.Params["city_id"])

	if(city.ID == 0){
		c.Ctx.Data["Message"] = "not_exist"
		c.RenderJSON(c.Ctx.Data, 422)
		return;
	}

	defer func(){
		if r := recover(); r != nil {
			c.Ctx.Data["Message"] = recover()
			c.RenderJSON(c.Ctx.Data, 403)
			return;
		}
	}()

	city.JoinUser(db, db_user)

	c.RenderJSON(city, 200)
}

func (c *Cities) LeaveCity()  {
	db := c.Ctx.DB.DB
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)

	db_user := appSession.GetCurrentUser(db)
	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	if(db_user.CityID == 0){
		c.Ctx.Data["Message"] = "not_exist"
		c.RenderJSON(c.Ctx.Data, 422)
		return;
	}

	defer func(){
		if r := recover(); r != nil {
			c.Ctx.Data["Message"] = recover()
			c.RenderJSON(c.Ctx.Data, 403)
			return;
		}
	}()

	db_user.City.LeaveUser(db, db_user)

	c.RenderJSON("", 200)
}

func (c *Cities) ValidateCityName()  {
	db := c.Ctx.DB.DB
	req := c.Ctx.Request()
	appSession := session.NewAppSession(req, c.Ctx.SessionStore)

	db_user := appSession.GetCurrentUser(db)
	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var city app_models.City
	helpers.RequestBodyToStruct(req.Body, &city)

	is_valid, msg := city.ValidateName(db)

	if(!is_valid) {
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = msg
		c.RenderJSON(c.Ctx.Data, 200)
		return;
	}

	c.Ctx.Data["Valid"] = true
	c.RenderJSON(c.Ctx.Data, 200)
	return;
}

func (c *Cities) ChangeName(){
	req := c.Ctx.Request()
	db := c.Ctx.DB.DB

	city := &app_models.City{}
	helpers.RequestBodyToStruct(req.Body, &city)

	city.WorldID = session.GetCurrentWorldIdByContext(c.Ctx)
	is_valid, msg := city.ValidateName(db)

	if(!is_valid) {
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = msg
		c.RenderJSON(c.Ctx.Data, 400)
		return;
	}

	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	if(!db_user.HasRole(db, "owner", "city", db_user.CityID)){
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = "not_permitted"
		c.RenderJSON(c.Ctx.Data, 403)
		return;
	}

	db_city := &app_models.City{}
	db.First(&db_city, "id = ?", db_user.CityID)

	var aka_array []string
	add_to_aka := true

	if(db_city.Aka != ""){
		helpers.JsonToStruct(db_city.Aka, &aka_array)
		add_to_aka = !helpers.StringInSlice(db_city.Name, aka_array)
	}

	if(add_to_aka){
		aka_array = append(aka_array, db_city.Name)
	}

	db_city.Name = city.Name
	db_city.Aka = helpers.StructToJson(aka_array)
	db.Save(&db_city)

	c.RenderJSON(db_city, 200)
}

func (c *Cities) GetCityLogs() {
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	user := appSession.GetCurrentUser(c.Ctx.DB.DB)

	if(user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	if(c.Ctx.Params["city_id"] != helpers.StructToJson(user.CityID)){
		c.Ctx.Data["Message"] = "not_permitted"
		c.RenderJSON(c.Ctx.Data, 403)
		return;
	}

	var cityLogs []app_models.UserLog
	c.Ctx.DB.Where("world_id = ?", world_id).Where("city_id = ?", c.Ctx.Params["city_id"]).Preload("User").Order("created_at desc").Limit(25).Find(&cityLogs)

	c.RenderJSON(cityLogs, 200)
}

func (c *Cities) GetCityUsers() {
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	user := appSession.GetCurrentUser(c.Ctx.DB.DB)

	if(user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	if(c.Ctx.Params["city_id"] != helpers.StructToJson(user.CityID)){
		c.Ctx.Data["Message"] = "not_permitted"
		c.RenderJSON(c.Ctx.Data, 403)
		return;
	}

	var cityUsers []app_models.User
	c.Ctx.DB.Where("world_id = ?", world_id).Where("city_id = ?", c.Ctx.Params["city_id"]).Preload("UserProgresses").Order("created_at desc").Limit(25).Find(&cityUsers)

	c.RenderJSON(cityUsers, 200)
}

func (c *Cities) PitchOutUser(){
	req := c.Ctx.Request()
	db := c.Ctx.DB.DB

	user_to_pitch := &app_models.User{}
	helpers.RequestBodyToStruct(req.Body, &user_to_pitch)

	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	if(!db_user.HasRole(db, "owner", "city", db_user.CityID)){
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = "not_permitted"
		c.RenderJSON(c.Ctx.Data, 403)
		return;
	}

	db_user_to_pitch := app_models.User{}
	db.Preload("City").First(&db_user_to_pitch, user_to_pitch.ID)

	db_user_to_pitch.City.LeaveUser(db, &db_user_to_pitch)

	c.RenderJSON("", 200)
}

func NewCities() controller.Controller {
	return &Cities{
		Routes: []string{
			"get;/api/v1/cities;GetCities",
			"get;/api/v1/current_city;GetCurrentCity",
			"post;/api/v1/cities;CreateCity",
			"post;/api/v1/city_join/{city_id};JoinCity",
			"post;/api/v1/city_leave;LeaveCity",
			"get;/api/v1/cities/{city_id}/logs;GetCityLogs",
			"get;/api/v1/cities/{city_id}/users;GetCityUsers",
			"post;/api/v1/city_validate;ValidateCityName",
			"post;/api/v1/city_change_name;ChangeName",
			"post;/api/v1/city_pitch_out;PitchOutUser",
		},
	}
}