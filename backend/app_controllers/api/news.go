package api

import (
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/app_models"
	"geek-pixel/backend/pixel_room"
	"geek-pixel/backend/session"
	"geek-pixel/backend/helpers"
)

type News struct {
	controller.BaseController
	Routes []string
}

func (c *News) GetNews()  {
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)

	var news_list []app_models.NewsFeed
	c.Ctx.DB.Preload("Creator").Preload("City").Preload("Clan").Where("world_id = ?", world_id).Order("created_at", true).Find(&news_list)

	c.RenderJSON(news_list, 200)
}

func (c *News) CreateNews()  {
	db := c.Ctx.DB.DB
	req := c.Ctx.Request()

	//TODO: validate permissions and AvailableAction

	db_user := session.GetCurrentUserByContext(c.Ctx)

	news := &app_models.NewsFeed{}
	helpers.RequestBodyToStruct(req.Body, &news)

	news.WorldID = db_user.WorldID
	news.CityID = db_user.CityID
	news.ClanID = db_user.ClanID
	news.CreatorID = db_user.ID
	db.Create(&news)

	var created_news app_models.NewsFeed
	db.Preload("Creator").Preload("City").Preload("Clan").Find(&created_news, news.ID)
	pixel_room.AddNews(int(db_user.ID), created_news)

	c.RenderJSON(created_news, 200)
}

func (c *News) GetLastUserNews()  {
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)
	current_user := session.GetCurrentUserByContext(c.Ctx)

	if(current_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var news app_models.NewsFeed
	c.Ctx.DB.Where("creator_id = ?", current_user.ID).Where("world_id = ?", world_id).Order("created_at", true).First(&news)

	c.RenderJSON(news, 200)
}

func NewNews() controller.Controller {
	return &News{
		Routes: []string{
			"get;/api/v1/news;GetNews",
			"post;/api/v1/news;CreateNews",
			"get;/api/v1/last_user_news;GetLastUserNews",
		},
	}
}