package api

import (
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/session"
	"geek-pixel/backend/helpers"
	"geek-pixel/backend/app_models"
)

type Clans struct {
	controller.BaseController
	Routes []string
}

func (c *Clans) GetClans()  {
	var clans_list []app_models.Clan
	c.Ctx.DB.Order("created_at").Where("world_id = ?", session.GetCurrentWorldIdByContext(c.Ctx)).Find(&clans_list)

	c.RenderJSON(clans_list, 200)
}

func (c *Clans) GetCurrentClan()  {
	db := c.Ctx.DB.DB
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	user := appSession.GetCurrentUser(c.Ctx.DB.DB)

	if(user == nil){
		c.RenderJSON("", 401)
	} else {
		clan := app_models.Clan{}
		db.Model(&user).Related(&clan)
		c.RenderJSON(clan, 200)
	}
}

func (c *Clans) ValidateClanName()  {
	db := c.Ctx.DB.DB
	req := c.Ctx.Request()
	appSession := session.NewAppSession(req, c.Ctx.SessionStore)

	db_user := appSession.GetCurrentUser(db)
	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var city app_models.City
	helpers.RequestBodyToStruct(req.Body, &city)

	is_valid, msg := city.ValidateName(db)

	if(!is_valid) {
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = msg
		c.RenderJSON(c.Ctx.Data, 200)
		return;
	}

	c.Ctx.Data["Valid"] = true
	c.RenderJSON(c.Ctx.Data, 200)
	return;
}

func (c *Clans) ChangeName(){
	req := c.Ctx.Request()
	db := c.Ctx.DB.DB

	clan := &app_models.Clan{}
	helpers.RequestBodyToStruct(req.Body, &clan)

	clan.WorldID = session.GetCurrentWorldIdByContext(c.Ctx)
	is_valid, msg := clan.ValidateName(db)

	if(!is_valid) {
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = msg
		c.RenderJSON(c.Ctx.Data, 400)
		return;
	}

	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	if(!db_user.HasRole(db, "owner", "clan", db_user.ClanID)){
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = "not_permitted"
		c.RenderJSON(c.Ctx.Data, 403)
		return;
	}

	db_clan := &app_models.Clan{}
	db.First(&db_clan, "id = ?", db_user.ClanID)

	var aka_array []string
	add_to_aka := true

	if(db_clan.Aka != ""){
		helpers.JsonToStruct(db_clan.Aka, &aka_array)
		add_to_aka = !helpers.StringInSlice(db_clan.Name, aka_array)
	}

	if(add_to_aka){
		aka_array = append(aka_array, db_clan.Name)
	}

	db_clan.Name = clan.Name
	db_clan.Aka = helpers.StructToJson(aka_array)
	db.Save(&db_clan)

	c.RenderJSON(db_clan, 200)
}

func (c *Clans) GetClanLogs() {
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	user := appSession.GetCurrentUser(c.Ctx.DB.DB)

	if(user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var clanLogs []app_models.UserLog
	c.Ctx.DB.Where("world_id = ?", world_id).Where("clan_id = ?", user.ClanID).Preload("User").Preload("City").Order("created_at desc").Limit(25).Find(&clanLogs)

	c.RenderJSON(clanLogs, 200)
}

func NewClans() controller.Controller {
	return &Clans{
		Routes: []string{
			"get;/api/v1/clans;GetClans",
			"get;/api/v1/current_clan;GetCurrentClan",
			"post;/api/v1/clan_validate;ValidateClanName",
			"post;/api/v1/clan_change_name;ChangeName",
			"get;/api/v1/clan_logs;GetClanLogs",
		},
	}
}