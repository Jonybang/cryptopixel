package api

import (
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/session"
	"geek-pixel/backend/app_models"
	"geek-pixel/backend/helpers"
)

type Users struct {
	controller.BaseController
	Routes []string
}

func (c *Users) GetCurrentUser()  {
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	user := appSession.GetCurrentUser(c.Ctx.DB.DB)

	if(user == nil){
		c.RenderJSON("", 401)
	} else {
		c.RenderJSON(user, 200)
	}
}

func (c *Users) GetUsers() {
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)

	var users []app_models.User
	c.Ctx.DB.Where("world_id = ?", world_id).Where("is_online = ?", true).Find(&users)

	c.RenderJSON(users, 200)
}

func (c *Users) GetUser() {
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)
	db := c.Ctx.DB.DB

	var user app_models.User
	db.Preload("City").Preload("Clan").Where("world_id = ? AND id = ?", world_id, c.Ctx.Params["user_id"]).First(&user)

	if(user.ReferID != 0){
		user.Refer = &app_models.User{}
		db.First(&user.Refer, "id = ?", user.ReferID)
	}

	c.RenderJSON(user, 200)
}

func (c *Users) ChangeName(){
	req := c.Ctx.Request()
	db := c.Ctx.DB.DB

	user := &app_models.User{}
	helpers.RequestBodyToStruct(req.Body, &user)

	user.WorldID = session.GetCurrentWorldIdByContext(c.Ctx)
	is_valid, msg := user.ValidateName(db)

	if(!is_valid) {
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = msg
		c.RenderJSON(c.Ctx.Data, 400)
		return;
	}

	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var aka_array []string
	add_to_aka := true

	if(db_user.Aka != ""){
		helpers.JsonToStruct(db_user.Aka, &aka_array)
		add_to_aka = !helpers.StringInSlice(db_user.Name, aka_array)
	}

	if(add_to_aka){
		aka_array = append(aka_array, db_user.Name)
	}

	db_user.Name = user.Name
	db_user.Aka = helpers.StructToJson(aka_array)
	db.Save(&db_user)

	c.RenderJSON(db_user, 200)
}

func (c *Users) ChangeEmail(){
	req := c.Ctx.Request()
	db := c.Ctx.DB.DB

	user := &app_models.User{}
	helpers.RequestBodyToStruct(req.Body, &user)

	user.WorldID = session.GetCurrentWorldIdByContext(c.Ctx)
	is_valid, msg := user.ValidateEmail(db)

	if(!is_valid) {
		c.Ctx.Data["Valid"] = false
		c.Ctx.Data["Message"] = msg
		c.RenderJSON(c.Ctx.Data, 400)
		return;
	}

	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	db_user.Email = user.Email
	db.Save(&db_user)

	c.RenderJSON(db_user, 200)
}

func (c *Users) AvailableAction() {
	current_user := session.GetCurrentUserByContext(c.Ctx)
	if(current_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}
	user_id := current_user.ID

	var available_action app_models.AvailableUserAction
	c.Ctx.DB.Where("user_id = ?", user_id).Where("action_type = ?", c.Ctx.Params["action_type"]).First(&available_action)

	c.RenderJSON(available_action, 200)
}

func (c *Users) GetUserLogs() {
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	user := appSession.GetCurrentUser(c.Ctx.DB.DB)

	if(user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var userLogs []app_models.UserLog
	c.Ctx.DB.Where("world_id = ?", world_id).Where("user_id = ?", user.ID).Order("created_at desc").Limit(25).Find(&userLogs)

	c.RenderJSON(userLogs, 200)
}

func (c *Users) NotJoinToRefer(){
	db := c.Ctx.DB.DB
	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	user := appSession.GetCurrentUser(db)

	if(user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	user.IsNotJoinToRefer = true
	db.Save(&user)
	c.RenderJSON("", 200)
}

func (c *Users) LogOut()  {
	session.Destroy(c.Ctx)
	c.RenderJSON("", 200)
}

func NewUsers() controller.Controller {
	return &Users{
		Routes: []string{
			"get;/api/v1/current_user;GetCurrentUser",
			"get;/api/v1/users;GetUsers",
			"get;/api/v1/users/{user_id};GetUser",
			"post;/api/v1/user_change_name;ChangeName",
			"post;/api/v1/user_change_email;ChangeEmail",
			"get;/api/v1/available_action/{action_type};AvailableAction",
			"get;/api/v1/user_logs;GetUserLogs",
			"post;/api/v1/not_join_to_refer;NotJoinToRefer",
			"post;/api/v1/log_out;LogOut",
		},
	}
}