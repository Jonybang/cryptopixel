package api

import (
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/session"
	"geek-pixel/backend/app_models"
)

type Roles struct {
	controller.BaseController
	Routes []string
}

func (c *Roles) GetCityRoles()  {
	db := c.Ctx.DB.DB
	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var roles []app_models.Role
	query := db.Where("level = ? AND city_id = ?", "city", db_user.CityID)
	query = query.Where("id IN (?)", db.Model(&app_models.UserRole{}).Where("user_id = ? AND city_id = ?", db_user.ID, db_user.CityID).Select("`role_id`").QueryExpr()).Find(&roles)

	c.RenderJSON(roles, 200)
}

func (c *Roles) GetClanRoles()  {
	db := c.Ctx.DB.DB
	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var roles []app_models.Role
	query := db.Where("level = ? AND clan_id = ?", "clan", db_user.ClanID)
	query = query.Where("id IN (?)", db.Model(&app_models.UserRole{}).Where("user_id = ? AND clan_id = ?", db_user.ID, db_user.ClanID).Select("`role_id`").QueryExpr()).Find(&roles)

	c.RenderJSON(roles, 200)
}

func (c *Roles) GetWorldRoles()  {
	db := c.Ctx.DB.DB
	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var roles []app_models.Role
	query := db.Where("level = ? AND world_id = ?", "world", db_user.WorldID)
	query = query.Where("id IN (?)", db.Model(&app_models.UserRole{}).Where("user_id = ? AND world_id = ?", db_user.ID, db_user.WorldID).Select("`role_id`").QueryExpr()).Find(&roles)

	c.RenderJSON(roles, 200)
}

func NewRoles() controller.Controller {
	return &Roles{
		Routes: []string{
			"get;/api/v1/city_roles;GetCityRoles",
			"get;/api/v1/clan_roles;GetClanRoles",
			"get;/api/v1/world_roles;GetWorldRoles",
		},
	}
}