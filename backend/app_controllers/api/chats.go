package api

import (
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/app_models"
	"geek-pixel/backend/pixel_room"
	"geek-pixel/backend/session"
	"geek-pixel/backend/helpers"
	"strconv"
)

type Chats struct {
	controller.BaseController
	Routes []string
}

func (c *Chats) GetChats()  {
	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		world_id := session.GetCurrentWorldIdByContext(c.Ctx)

		var chats_list []app_models.Chat
		c.Ctx.DB.Where("world_id = ?", world_id).Where("type = ?", "global").Order("created_at").Find(&chats_list)

		c.RenderJSON(chats_list, 200)
		return
	}

	var chats_member_list []app_models.ChatMember
	c.Ctx.DB.Preload("Chat").Where("user_id = ?", db_user.ID).Order("created_at").Find(&chats_member_list)

	var chats_list []*app_models.Chat
	for index, member := range chats_member_list {
		_ = index
		chats_list = append(chats_list, member.Chat)
	}
	c.RenderJSON(chats_list, 200)
}

func (c *Chats) GetChatMembers()  {
	db := c.Ctx.DB.DB
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)

	chat := app_models.Chat{}
	db.First(&chat, "id = ?", c.Ctx.Params["chat_id"])
	if(chat.ID == 0 || chat.WorldID != world_id) {
		c.RenderJSON([]string{}, 200)
		return
	}

	var members_list []app_models.ChatMember
	c.Ctx.DB.Preload("User").Where("chat_id = ?", chat.ID).Order("created_at", true).Find(&members_list)

	c.RenderJSON(members_list, 200)
}

func (c *Chats) GetChatMessages()  {
	db := c.Ctx.DB.DB
	world_id := session.GetCurrentWorldIdByContext(c.Ctx)

	chat := app_models.Chat{}
	db.First(&chat, "id = ?", c.Ctx.Params["chat_id"])
	if(chat.ID == 0 || chat.WorldID != world_id) {
		c.RenderJSON([]string{}, 200)
		return
	}

	var messages_list []app_models.ChatMessage
	c.Ctx.DB.Preload("User").Where("chat_id = ?", c.Ctx.Params["chat_id"]).Order("created_at", true).Find(&messages_list)

	c.RenderJSON(messages_list, 200)
}

func (c *Chats) CreateChatMessage()  {
	db := c.Ctx.DB.DB
	req := c.Ctx.Request()

	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)
	db_user := session.GetCurrentUserByContext(c.Ctx)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	message := &app_models.ChatMessage{}
	helpers.RequestBodyToStruct(req.Body, &message)

	chat_id, err := strconv.ParseUint(c.Ctx.Params["chat_id"], 10, 64)
	if(err != nil){
		c.Ctx.Data["Message"] = "chat_id invalid"
		c.RenderJSON(c.Ctx.Data, 500)
		return;
	}
	message.ChatID = uint(chat_id)

	chat := &app_models.Chat{}
	db.First(&chat, message.ChatID)

	if(chat.WorldID != appSession.GetCurrentWorldId()){
		c.Ctx.Data["Message"] = "not permitted"
		c.RenderJSON(c.Ctx.Data, 500)
		return;
	}

	message.UserID = db_user.ID
	db.Create(&message)

	var created_message app_models.ChatMessage
	db.Preload("User").Preload("Chat").Find(&created_message, message.ID)
	pixel_room.AddMessage(int(db_user.ID), created_message)

	c.RenderJSON(created_message, 200)
}

func NewChats() controller.Controller {
	return &Chats{
		Routes: []string{
			"get;/api/v1/chats;GetChats",
			"get;/api/v1/chat/{chat_id}/messages;GetChatMessages",
			"get;/api/v1/chat/{chat_id}/members;GetChatMembers",
			"post;/api/v1/chat/{chat_id}/messages;CreateChatMessage",
		},
	}
}