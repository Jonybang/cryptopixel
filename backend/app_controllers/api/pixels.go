package api

import (
	"github.com/oleiade/reflections"
	"github.com/gernest/utron/controller"
	"geek-pixel/backend/session"
	"geek-pixel/backend/helpers"
	"geek-pixel/backend/app_models"
	"strconv"
	"strings"
)

type Pixels struct {
	controller.BaseController
	Routes []string
}

func (c *Pixels) GetPixelTypes()  {
	var pixel_types []app_models.PixelType
	c.Ctx.DB.Find(&pixel_types)

	c.RenderJSON(pixel_types, 200)
}

func (c *Pixels) GetPixels()  {
	query := c.Ctx.Request().URL.Query()

	area := helpers.AreaStruct{}
	fields := [4]string{"StartX", "EndX", "StartY", "EndY"}

	for index, field := range fields {
		_ = index
		int_value, err := strconv.Atoi(query.Get(field))
		if(err != nil){
			c.Ctx.Data["Message"] = err
			c.RenderJSON(c.Ctx.Data, 400)
			return;
		}
		_ = reflections.SetField(&area, strings.Title(field), int_value)
	}

	appSession := session.NewAppSession(c.Ctx.Request(), c.Ctx.SessionStore)

	world_id := appSession.GetCurrentWorldId()
	world_id_str := helpers.StructToJson(world_id)

	appSession.Set(world_id_str + "_area", area)
	appSession.Save(c.Ctx.Response())

	var pixels []app_models.Pixel
	c.Ctx.DB.Preload("Type").Where("world_id = ?", world_id).Where("x >= ?", area.StartX).Where("x <= ?", area.EndX).Where("y >= ?", area.StartY).Where("y <= ?", area.EndY).Find(&pixels)

	c.RenderJSON(pixels, 200)
}

func (c *Pixels) CreatePixel()  {
	db := c.Ctx.DB.DB
	req := c.Ctx.Request()
	appSession := session.NewAppSession(req, c.Ctx.SessionStore)
	db_user := appSession.GetCurrentUser(db)

	if(db_user == nil){
		c.Ctx.Data["Message"] = "unauthorized"
		c.RenderJSON(c.Ctx.Data, 401)
		return;
	}

	var available_action app_models.AvailableUserAction
	c.Ctx.DB.Where("user_id = ?", db_user.ID).Where("action_type = ?", "add_pixel").First(&available_action)
	if(available_action.Value < 1){
		c.Ctx.Data["Message"] = "not_available"
		c.RenderJSON(c.Ctx.Data, 423)
		return;
	}

	defer func() {
		if r := recover(); r != nil {
			c.Ctx.Data["Message"] = r
			c.RenderJSON(c.Ctx.Data, 403)
			return;
		}
	}()
	var pixel app_models.Pixel
	helpers.RequestBodyToStruct(req.Body, &pixel)
	pixel = db_user.AddPixel(db, pixel.X, pixel.Y, pixel.TypeID)

	c.RenderJSON(pixel, 200)
	return;
}

func NewPixels() controller.Controller {
	return &Pixels{
		Routes: []string{
			"get;/api/v1/pixel_types;GetPixelTypes",
			"get;/api/v1/pixels;GetPixels",
			"post;/api/v1/pixels;CreatePixel",
		},
	}
}