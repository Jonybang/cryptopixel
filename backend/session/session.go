package session

import (
	"github.com/gorilla/sessions"
	"net/http"
	"github.com/jinzhu/gorm"
	"github.com/gernest/utron/base"
	"geek-pixel/backend/app_models"
)

type AppSession struct {
	Store sessions.Store;
	Session *sessions.Session;
	Request *http.Request;
}

func NewAppSession(request *http.Request, store sessions.Store) AppSession {
	s := AppSession{Request: request, Store: store}
	return s;
}

func (s *AppSession) GetSession() *sessions.Session {
	//if(s.Session != nil){
	//	return s.Session
	//}

	_session, _err := s.Store.Get(s.Request, "cryptopixel")
	if _err != nil {
		panic(_err)
	}
	s.Session = _session
	return s.Session
}

func (s *AppSession) Get(key string) interface {} {
	s.GetSession()
	if val, ok := s.Session.Values[key]; ok {
		return val
	}
	return nil
}

func (s *AppSession) Set(key string, value interface {}) {
	s.GetSession()
	s.Session.Values[key] = value
}
func (s *AppSession) Save(response http.ResponseWriter) {
	s.GetSession()
	err := s.Session.Save(s.Request, response)
	if(err != nil){
		panic(err)
	}
}

func (s *AppSession) SetCurrentUser(user *app_models.User) {
	s.GetSession()
	s.Session.Values["current_user_id"] = user.ID
}

func (s *AppSession) GetCurrentUserId() int {
	s.GetSession()
	val := s.Session.Values["current_user_id"]
	if(val == nil){
		return 0
	} else {
		return int(val.(uint))
	}
}

func (s *AppSession) GetCurrentUser(db *gorm.DB) *app_models.User {
	s.GetSession()
	user_id := s.GetCurrentUserId()
	if(user_id == 0){
		return nil
	}
	user := &app_models.User{}
	db.Preload("Roles").Preload("UserProgresses").Preload("City").Preload("Clan").First(&user, user_id)

	if(user.ReferID != 0){
		user.Refer = &app_models.User{}
		db.First(&user.Refer, "id = ?", user.ReferID)
	}

	if(user.ID == 0){
		return nil
	}
	return user
}

func (s *AppSession) SetCurrentWorld(world *app_models.World) {
	s.GetSession()
	s.Session.Values["current_world_id"] = world.ID
}

func (s *AppSession) GetCurrentWorldId() uint {
	s.GetSession()
	val := s.Session.Values["current_world_id"]
	if(val == nil){
		return 0
	} else {
		return uint(val.(uint))
	}
}

func (s *AppSession) GetCurrentWorld(db *gorm.DB) *app_models.World {
	s.GetSession()
	world_id := s.GetCurrentWorldId()

	world := &app_models.World{}

	if(world_id == 0){
		current_user := s.GetCurrentUser(db)
		if(current_user != nil){
			world_id = current_user.WorldID
			db.First(&world, world_id)
			s.SetCurrentWorld(world)
		}
	}
	if(world_id == 0){
		return nil
	}

	if(world.ID == 0){
		db.First(&world, world_id)
	}

	if(world.ID == 0){
		return nil
	}
	return world
}

func GetCurrentUserByContext(ctx *base.Context) *app_models.User {
	db := ctx.DB.DB
	req := ctx.Request()

	appSession := NewAppSession(req, ctx.SessionStore)
	return appSession.GetCurrentUser(db)
}

func GetCurrentWorldIdByContext(ctx *base.Context) uint {
	req := ctx.Request()

	appSession := NewAppSession(req, ctx.SessionStore)
	current_world_id := appSession.Get("current_world_id")
	if(current_world_id == nil){
		return 0
	} else {
		return current_world_id.(uint)
	}
}

func Destroy(ctx *base.Context)  {
	appSession := NewAppSession(ctx.Request(), ctx.SessionStore)
	appSession.Set("current_world_id", nil)
	appSession.Set("current_user_id", nil)
	appSession.Save(ctx.Response())
}