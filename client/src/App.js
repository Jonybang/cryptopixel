import Vue from 'vue';
import {Modal} from "./directives/AsyncModal";
import Locale from "./services/locale";
import {Tabs, Tab} from "../vendor/tabs";
import JoinReferModal from "./components/JoinReferModal/JoinReferModal.vue";
// import {install as VueTableInstall} from 'vuetable-2';
import Vuex from 'vuex';
import * as _ from 'lodash';

import moment from 'moment';
import Notifications from 'vue-notification'

import {
  EventBus,
  EVENT_PIXEL_WEBSOCKET_MESSAGE,
  EVENT_CHAT_WEBSOCKET_MESSAGE,
  EVENT_NEWS_WEBSOCKET_MESSAGE,
  EVENT_JOIN_LEAVE_WEBSOCKET_MESSAGE,
  EVENT_USER_AUTHORIZED,
  EVENT_AVAILABLE_ACTION_WEBSOCKET_MESSAGE,
  EVENT_CITY_WEBSOCKET_MESSAGE,
  EVENT_CITY_USER_WEBSOCKET_MESSAGE,
  EVENT_CHAT_USER_WEBSOCKET_MESSAGE,
  EVENT_PROGRESS_USER_WEBSOCKET_MESSAGE
} from "./services/events";

import storePlugin from './services/store.plugin.js';
import httpPlugin from './services/http.plugin.js';
import Loader from "./directives/Loader/Loader";
import EditField from "./directives/EditField/EditField";
import {Helper} from "./services/helper";

Vue.use(Notifications);

Vue.use(httpPlugin);
Vue.use(Vuex);
Vue.use(storePlugin);

Vue.component('modal', Modal);
Vue.component('tabs', Tabs);
Vue.component('tab', Tab);
Vue.component('loader', Loader);
Vue.component('edit-field', EditField);

// VueTableInstall(Vue);

Vue.filter('beautyDate', function (date) {
  let mDate = moment(date);
  let now = moment();

  if(now.diff(mDate, 'hours') >= 24)
    return mDate.format("D MMMM YYYY H:mm:ss");
  else
    return mDate.fromNow();
});

Vue.filter('locale', function (key) {
  return Locale.get(key);
});

export default {
  name: 'app',
  created() {
    if (!this.$root.$websocket)
      this.initWebsocket();

    this.$http.initAuthCheck(this);

    this.getCurrentWorld().then(()=>{
      this.getCurrentUser();
    }, () => {
      this.getCurrentUser();
    });

    EventBus.$on(EVENT_USER_AUTHORIZED, () => {
      this.destroyWebsocket();

      this.initWebsocket();

      this.getCurrentUser();
    });

    EventBus.$on(EVENT_CITY_WEBSOCKET_MESSAGE, (message_data) => {
      if(!this.current_user)
        return;

      if(message_data.Type === "add_city" && message_data.Data && message_data.Data.OwnerID === this.current_user.ID){
        this.getCurrentUser();
      }
    });

    EventBus.$on(EVENT_CITY_USER_WEBSOCKET_MESSAGE, (message_data) => {
      if(!this.current_user)
        return;

      if(_.includes(['join_city', 'leave_city'], message_data.Type) && message_data.Data && message_data.Data.ID === this.current_user.ID){
        this.getCurrentUser();
      }
    });

    EventBus.$on(EVENT_PROGRESS_USER_WEBSOCKET_MESSAGE, (message_data) => {
      if(!_.includes(['level', 'experience'], message_data.Data.Type))
        return;

      let user = _.clone(this.current_user);

      Helper.updateUserProgress(user, message_data.Data);

      this.$store.commit('current_user', user);
    });
  },

  beforeDestroy() {
    this.destroyWebsocket();
  },

  methods: {
    getCurrentWorld(){
      return this.$http.get('current_world')
        .then((response) => {
          this.$store.commit('current_world', response.data);
        })
    },

    getCurrentUser(){
      this.$store.commit('current_user_loaded', false);

      return this.$http.current_user()
        .then((response) => {
          this.$store.commit('current_user_loaded', true);

          if (!response.data || !response.data.ID){
            this.currentUserNotFound();
            return;
          }

          //this.$router.push('main-page');

          let user = response.data;

          Helper.updateUserProgressesList(user);

          this.$store.commit('current_user', user);
          this.$store.commit('current_user_loaded', true);

          if(!this.current_user.CityID && this.current_user.Refer && this.current_user.Refer.CityID && !this.current_user.IsNotJoinToRefer){
            this.joinReferModal();
          }

          return this.current_user;
        }, (response) => {
          this.$store.commit('current_user_loaded', true);
          this.currentUserNotFound();
          return response;
        });
    },

    currentUserNotFound(){
      if(!this.current_world || !this.current_world.ID){
        this.$router.push('/worlds');
      } else if(this.$router.currentRoute.name === 'world') {
        this.$router.push('/game');
      }
    },

    initWebsocket(){
      this.$root.$websocket = new WebSocket('ws://' + window.document.location.host + '/websocket/socket');
      this.$root.$websocket.onmessage = (event) => {
        const message_data = JSON.parse(event.data);
        console.log('$websocket.onmessage', message_data);

        if (message_data.Data)
          message_data.Data = JSON.parse(message_data.Data);

        switch (message_data.Type) {
          case 'add_pixel':
            EventBus.$emit(EVENT_PIXEL_WEBSOCKET_MESSAGE, message_data);
            break;

          case 'add_message':
            EventBus.$emit(EVENT_CHAT_WEBSOCKET_MESSAGE, message_data);
            break;

          case 'add_news':
            EventBus.$emit(EVENT_NEWS_WEBSOCKET_MESSAGE, message_data);
            break;

          case 'change_available_action':
            EventBus.$emit(EVENT_AVAILABLE_ACTION_WEBSOCKET_MESSAGE, message_data);
            break;

          case 'add_city':
          case 'change_city':
            EventBus.$emit(EVENT_CITY_WEBSOCKET_MESSAGE, message_data);
            break;

          case 'leave_city':
          case 'join_city':
            EventBus.$emit(EVENT_CITY_USER_WEBSOCKET_MESSAGE, message_data);
            break;

          case 'join_chat':
            EventBus.$emit(EVENT_CHAT_USER_WEBSOCKET_MESSAGE, message_data);
            break;

          case 'update_progress':
            EventBus.$emit(EVENT_PROGRESS_USER_WEBSOCKET_MESSAGE, message_data);
            break;

          case 'error':
            if(message_data.Data === "not_permitted:add_city"){
              this.$store.commit('hide_create_or_join_city', false);
            }
            this.$notify({
              type: 'error',
              title: Locale.get('error') + '!',
              text: message_data.Data
            });
            break;

          case 'join':
          case 'leave':
            EventBus.$emit(EVENT_JOIN_LEAVE_WEBSOCKET_MESSAGE, message_data);
            break;
        }
      };

      this.$root.$websocket.onclose = () => {
        console.log('websocket reconnect...');
        setTimeout(() => {this.initWebsocket();}, 1000);
      };
    },
    destroyWebsocket(){
      if(this.$root.$websocket){
        this.$root.$websocket.onclose = null;
        this.$root.$websocket.close();
      }
    },
    joinReferModal(){
      this.$root.$asyncModal.open({
        id: 'join-refer-modal',
        component: JoinReferModal,
        options: {
          destroy: true
        }
      });
    }
  },

  mounted() {
    this.$root.$asyncModal = this.$refs.modal;
    this.$root.$asyncSubModal = this.$refs.sub_modal;
  },
  computed: {
    current_world () {
      return this.$store.state.current_world
    },
    current_user () {
      return this.$store.state.current_user
    },
    current_user_loaded () {
      return this.$store.state.current_user_loaded
    }
  },
  data() {
    return {}
  },
}
