import Ribbon from "../Ribbon/Ribbon.vue";

export default {
  name: 'market-page',
  components: {
    'ribbon': Ribbon
  },
  created() {

  },
  mounted() {

  },
  methods: {

  },
  data: function () {
    return {

    };
  },
  computed: {
    current_user () {
      return this.$store.state.current_user
    }
  },
}
