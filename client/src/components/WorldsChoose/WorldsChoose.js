export default {
  name: 'worlds-choose',
  created() {

  },
  mounted() {
    this.getWorlds();
  },
  methods: {
    getWorlds(){
      this.$http.get('worlds')
        .then((response) => {
          this.worlds = response.data;
        });
    },
    setWorld(world){
      this.$http.post('set_world/' + world.ID).then((response)=>{
        this.$store.commit('current_world', response.data);
        this.$router.push('game');
      })

      if(world.Language === "en")
        window.location = '/';
      else
        window.location = '/' + world.Language + '/';
    }
  },
  data: function () {
    return {
      worlds: []
    };
  },
  computed: {

  },
}
