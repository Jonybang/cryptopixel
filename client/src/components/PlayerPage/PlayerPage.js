import Ribbon from "../Ribbon/Ribbon.vue";
import * as _ from 'lodash';
import {Helper} from "../../services/helper";
import Locale from "../../services/locale";

export default {
  name: 'player-page',
  components: {
    'ribbon': Ribbon
  },
  created() {
    this.$http.get('user_logs').then((response) => {
      this.user_logs = response.data
    });
    this.$http.get('current_user').then((response) => {
      this.user = response.data
      Helper.updateUserProgressesList(this.user);
    });
  },
  mounted() {

  },
  methods: {
    toggleEdit(field){
      let field_state = this[field + '_state'];
      if(field_state.edit)
        this.user[field] = field_state.old_value;
      else
        field_state.old_value = this.user[field];

      field_state.edit = !field_state.edit;
    },
    saveField(field){
      this.$http.post("user_change_" + _.snakeCase(field), this.user).then((response)=> {
        if(response && response.status === 200){
          this.$notify({
            title: Locale.get('success'),
            text: field + ' ' + Locale.get('success_changed')
          });
          this[field + '_state'].edit = false;
        }
      });
    }
  },
  data: function () {
    return {
      user: {},
      Name_state: {
        valid: true,
        loading: false,
        edit: false,
        error_message: "",
        old_value: ""
      },
      Email_state: {
        valid: true,
        loading: false,
        edit: false,
        error_message: "",
        old_value: ""
      },
      user_logs: []
    };
  },
  computed: {
    current_user () {
      return this.$store.state.current_user;
    }
  },
}
