import Ribbon from "../Ribbon/Ribbon.vue";
import * as _ from 'lodash';
import Locale from "../../services/locale";

export default {
  name: 'clan-page',
  components: {
    'ribbon': Ribbon
  },
  created() {
    this.$http.get('current_clan').then((response) => {
      this.clan = response.data;
    });

    this.$http.get('clan_roles').then((response) => {
      this.clan_roles = response.data;
    });

    this.$http.get('clan_logs').then((response) => {
      this.clan_logs = response.data;
    });
  },
  mounted() {

  },
  methods: {
    toggleEdit(field){
      let field_state = this[field + '_state'];
      if(field_state.edit)
        this.clan[field] = field_state.old_value;
      else
        field_state.old_value = this.clan[field];

      field_state.edit = !field_state.edit;
    },
    saveField(field){
      this.$http.post("clan_change_" + _.snakeCase(field), this.clan).then((response)=> {
        if(response && response.status === 200){
          this.$notify({
            title: Locale.get('success'),
            text: field + ' ' + Locale.get('success_changed')
          });
          this[field + '_state'].edit = false;
        }
      });
    }
  },
  data: function () {
    return {
      clan: {},
      clan_roles: [],
      clan_logs: [],
      Name_state: {
        valid: true,
        loading: false,
        edit: false,
        error_message: "",
        old_value: ""
      },
    };
  },
  computed: {
    current_user () {
      return this.$store.state.current_user;
    },
    can_edit(){
      return this.clan_roles.some((role) => role.Key === 'owner');
    }
  },
}
