import * as _ from 'lodash';

export default {
  name: 'city-users',
  created() {
    this.$http.get('city_roles').then((response) => {
      this.city_roles = response.data;
    });
    this.getUsers();
  },
  mounted() {

  },
  methods: {
    getUsers(){
      this.$http.get('cities/' + this.current_user.CityID + '/users').then((response) => {
        this.city_users = response.data;
      });
    },
    pitchOut(user){
      if(!confirm('Are you sure?'))
        return false;

      this.$http.post("city_pitch_out", user).then((response)=> {
        this.getUsers();
      });
      return false;
    }
  },
  data: function () {
    return {
      city_roles: [],
      city_users: []
    };
  },
  computed: {
    current_user () {
      return this.$store.state.current_user;
    },
    can_edit(){
      return this.city_roles.some((role) => role.Key === 'owner');
    }
  }
}
