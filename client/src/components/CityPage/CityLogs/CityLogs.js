import * as _ from 'lodash';

export default {
  name: 'city-logs',
  created() {
    this.$http.get('cities/' + this.current_user.CityID + '/users').then((response) => {
      this.city_logs = response.data;
    });
  },
  mounted() {

  },
  methods: {

  },
  data: function () {
    return {
      city_logs: [],
    };
  },
  computed: {
    current_user () {
      return this.$store.state.current_user;
    }
  },
}
