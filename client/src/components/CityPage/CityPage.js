import Ribbon from "../Ribbon/Ribbon.vue";
import * as _ from 'lodash';
import {Helper} from "../../services/helper";
import Locale from "../../services/locale";

export default {
  name: 'city-page',
  components: {
    'ribbon': Ribbon
  },
  created() {
    this.getCity();
  },
  mounted() {

  },
  methods: {
    toggleEdit(field){
      let field_state = this[field + '_state'];
      if(field_state.edit)
        this.city[field] = field_state.old_value;
      else
        field_state.old_value = this.city[field];

      field_state.edit = !field_state.edit;
    },
    saveField(field){
      this.$http.post("city_change_" + _.snakeCase(field), this.city).then((response)=> {
        if(response && response.status === 200){
          this.$notify({
            title: Locale.get("success"),
            text: field + ' ' + Locale.get("success_changed")
          });
          this[field + '_state'].edit = false;
        }
      });
    },
    getCity(){
      this.$http.get('current_city').then((response) => {
        this.city = response.data;
      });

      this.$http.get('city_roles').then((response) => {
        this.city_roles = response.data;
      });
    },
    leaveCity(){
      this.$http.get('available_action/join_city')
        .then((response) => {
          if(!response)
            return;

          let availableJoin = response.data;

          if(availableJoin.Value > 0){
            if(!confirm(Locale.get("are_you_sure")))
              return;
          } else {
            if(!confirm(Locale.get("city_join_available_warning") + " " + Helper.humanDate(availableJoin.AvailableOn) + ". " + Locale.get("are_you_sure")))
              return;
          }

          this.$http.post("city_leave").then((response) => {
            this.getCity();
          })
        });
    }
  },
  data: function () {
    return {
      city: {},
      city_roles: [],
      city_logs: [],
      Name_state: {
        valid: true,
        loading: false,
        edit: false,
        error_message: "",
        old_value: ""
      },
    };
  },
  computed: {
    current_user () {
      return this.$store.state.current_user
    },
    can_edit(){
      return this.city_roles.some((role) => role.Key === 'owner')
    }
  },
}
