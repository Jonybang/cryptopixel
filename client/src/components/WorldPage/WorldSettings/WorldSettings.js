import * as _ from 'lodash';
import Locale from "../../../services/locale";

export default {
  name: 'world-settings',
  created() {
    this.$http.get('world_roles').then((response) => {
      this.world_roles = response.data;
    });
    this.$http.get('world_settings').then((response) => {
      this.world_settings = response.data;
    });
  },
  mounted() {

  },
  methods: {
    toggleEdit(setting){
      let field_state = this.Setting_state;

      if(!field_state.object || this.Setting_state.field === setting.Name){
        field_state.edit = !field_state.edit;
      } else if(field_state.object) {
        field_state.object.Value = field_state.old_value;
        this.world_settings = _.clone(this.world_settings);
      }

      this.Setting_state.field = setting.Name;
      this.Setting_state.object = setting;

      if(field_state.edit)
        field_state.old_value = setting.Value;
      else
        setting.Value = field_state.old_value;
    },
    saveSetting(setting){
      this.$http.post("world_change_setting", setting).then((response)=> {
        if(response && response.status === 200){
          this.$notify({
            title: Locale.get('success'),
            text: Locale.get('setting_success_changed')
          });
          this.Setting_state.edit = false;
        }
      });
    }
  },
  data: function () {
    return {
      world_settings: [],
      world_roles: [],
      Setting_state: {
        field: "",
        valid: true,
        loading: false,
        edit: false,
        error_message: "",
        old_value: "",
        object: null
      }
    };
  },
  computed: {
    current_user () {
      return this.$store.state.current_user;
    },
    can_edit(){
      return this.world_roles.some((role) => role.Key === 'super_admin');
    }
  }
}
