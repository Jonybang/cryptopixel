import Ribbon from "../Ribbon/Ribbon.vue";
import * as _ from 'lodash';
import Locale from "../../services/locale";

export default {
  name: 'world-page',
  components: {
    'ribbon': Ribbon
  },
  created() {
    this.$http.get('current_world').then((response) => {
      this.world = response.data;
    });

    this.$http.get('world_logs').then((response) => {
      this.world_logs = response.data;
    });

    this.$http.get('world_settings').then((response) => {
      this.world_settings = response.data;
    });
  },
  mounted() {

  },
  methods: {
    toggleEdit(field){
      let field_state = this[field + '_state'];
      if(field_state.edit)
        this.world[field] = field_state.old_value;
      else
        field_state.old_value = this.world[field];

      field_state.edit = !field_state.edit;
    },
    saveField(field){
      this.world.MaxX = parseInt(this.world.MaxX);
      this.world.MaxY = parseInt(this.world.MaxY);

      this.$http.post("world_change_" + (field === "Name" ? "name" : "property"), this.world).then((response)=> {
        if(response && response.status === 200){
          this.$notify({
            title: Locale.get("success"),
            text: field + ' ' + Locale.get("success_changed")
          });
          this[field + '_state'].edit = false;
        }
      });
    },
    exitWorld(){
      this.$http.post("log_out").then(() => {
        this.$root.$websocket.onclose = null;
        this.$root.$websocket.close();
        this.$router.push('/worlds');
      });
    }
  },
  data: function () {
    return {
      world: {},
      world_roles: [],
      world_logs: [],
      world_settings: [],
      Name_state: {
        valid: true,
        loading: false,
        edit: false,
        error_message: "",
        old_value: ""
      },
      MaxX_state: {
        valid: true,
        loading: false,
        edit: false,
        error_message: "",
        old_value: ""
      },
      MaxY_state: {
        valid: true,
        loading: false,
        edit: false,
        error_message: "",
        old_value: ""
      }
    };
  },
  computed: {
    current_user () {
      let user = this.$store.state.current_user;
      if(user && user.ID){
        this.$http.get('world_roles').then((response) => {
          this.world_roles = response.data;
        });
      }
      return user;
    },
    can_edit(){
      return this.world_roles.some((role) => role.Key === 'super_admin');
    }
  },
}
