import * as _ from 'lodash';

export default {
  name: 'world-logs',
  created() {
    this.$http.get('world_logs').then((response) => {
      this.world_logs = response.data;
    });
  },
  mounted() {

  },
  methods: {

  },
  data: function () {
    return {
      world_logs: [],
    };
  },
  computed: {
    current_user () {
      return this.$store.state.current_user;
    }
  },
}
