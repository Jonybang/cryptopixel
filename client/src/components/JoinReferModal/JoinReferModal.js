import {ModalItem} from '../../directives/AsyncModal'

export default {
  components: {
    ModalItem
  },
  created(){
    this.$http.get('users/' + this.current_user.ReferID).then((response) => {
      this.referral = response.data;
    });
  },
  methods: {
    ok () {
      this.$http.post('city_join/' + this.referral.CityID).then((response) => {
        this.$root.$asyncModal.close('join-refer-modal');
      });
    },
    cancel () {
      this.$http.post('not_join_to_refer').then((response) => {
        this.$root.$asyncModal.close('join-refer-modal');
      });
    }
  },
  watch: {

  },
  data: function () {
    return {
      referral: {
        City: {},
        Clan: {}
      }
    }
  },
  computed: {
    current_user () {
      return this.$store.state.current_user
    }
  }
}
