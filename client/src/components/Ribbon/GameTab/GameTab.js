import {
    EventBus,
    EVENT_MAIN_TOOL_UNSELECTED,
    EVENT_MAIN_TOOL_SELECTED,
    EVENT_MAIN_PIXEL_TYPE_SELECTED,
    EVENT_USER_AUTHORIZED,
    EVENT_PIXEL_WEBSOCKET_MESSAGE,
    EVENT_AVAILABLE_ACTION_WEBSOCKET_MESSAGE,
    EVENT_MAIN_TOOL_RESET
} from '../../../services/events';

import moment from 'moment';
import Locale from "../../../services/locale";

export default {
    name: 'GameTab',
    mounted() {
        this.$http.get('pixel_types')
            .then((response) => {
                this.pixelTypes = response.data;

                this.setDefaultToolsState();
            }, (response) => {
                console.log('pixel_types error', response);
            });

        this.getAvailablePixels();

        EventBus.$on(EVENT_USER_AUTHORIZED, () => {
            this.getAvailablePixels();
        });

        EventBus.$on(EVENT_PIXEL_WEBSOCKET_MESSAGE, (message_data) => {
            if(message_data.Type !== "add_pixel")
                return;

            if(!message_data.Data || !this.current_user || message_data.Data.UserID !== this.current_user.ID)
                return;

            this.getAvailablePixels();
        });

        EventBus.$on(EVENT_AVAILABLE_ACTION_WEBSOCKET_MESSAGE, () => {
            this.getAvailablePixels();
        });

        window.addEventListener('keydown', this.keypress.bind(this));

        if (!this.ribbon_state) {
            this.ribbon_state = {};
            this.$store.commit('ribbon_state', this.ribbon_state);
        }

        window.setInterval(() => {
            this.now = new Date();
        },1000);

        EventBus.$on(EVENT_MAIN_TOOL_RESET, () => {
            this.selectTool(this.mainTools[0].name);
        });

        EventBus.$on(EVENT_MAIN_TOOL_SELECTED, (tool_name) => {
            if(tool_name === this.activeTool)
                return;

            if(tool_name === 'add_city'){
                this.$notify({
                    title: Locale.get('select_location'),
                    text: Locale.get('where_location')
                });
            }
            this.selectTool(tool_name);
        });
    },
    methods: {
        selectTool(tool_name) {
            this.activeTool = tool_name;

            const last_pixel_type = this.pixelTypes[this.pixelTypes.length - 1];

            if (tool_name === 'add_pixel' && (!this.activePixelType || this.activePixelType === last_pixel_type.ID))
                this.selectPixelType(this.pixelTypes[0]);

            if (tool_name === 'remove_pixel' && (!this.activePixelType || this.activePixelType !== last_pixel_type.ID))
                this.selectPixelType(last_pixel_type);

            if(tool_name === 'add_pixel')
                this.sideNavOpened = true;

            EventBus.$emit(EVENT_MAIN_TOOL_SELECTED, tool_name);
        },
        isToolActive(tool_name) {
            return this.activeTool === tool_name;
        },
        selectPixelType(type) {
            this.activePixelType = type.ID;

            if(this.activeTool !== 'add_pixel' && type.Key !== 'empty')
                this.selectTool('add_pixel');

            this.sideNavOpened = false;

            EventBus.$emit(EVENT_MAIN_PIXEL_TYPE_SELECTED, type);
        },
        isPixelTypeActive(type) {
            return this.activePixelType === type.ID;
        },
        makeAction(action){
            switch (action.name) {
                // case 'save':
                //     EventBus.$emit(EVENT_PROJECT_SAVE);
                //     break;
                // case 'accidents':
                //     this.ribbon_state.is_draw_accidents = !this.ribbon_state.is_draw_accidents;
                //     this.$store.commit('ribbon_state', this.ribbon_state);
                //
                //     if(this.ribbon_state.is_draw_accidents){
                //         EventBus.$emit(EVENT_PROJECT_DRAW_ACCIDENTS);
                //         action.inverse = true;
                //     } else {
                //         EventBus.$emit(EVENT_PROJECT_HIDE_ACCIDENTS);
                //         action.inverse = false;
                //     }
                //     break;
            }
        },
        keypress(e){
            if (e.keyCode == 27 && this.activeTool)
                this.selectTool(this.activeTool);
        },
        setDefaultToolsState(){
            this.selectTool(this.mainTools[0].name);
        },
        getAvailablePixels(){
            this.$http.get('available_action/add_pixel')
                .then((response) => {
                    if(!response)
                        return;

                    this.availablePixels = response.data;
                });
        }
    },
    computed: {
        current_user () {
            return this.$store.state.current_user
        },
        untilAvailablePixel(){
            this.now;
            let mDate = moment(this.availablePixels.AvailableOn);
            return mDate.fromNow();
        }
    },
    data() {
        return {
            mainTools: [
                {name: 'move', icon: 'move'},
                {name: 'add_pixel', icon: 'plus'},
                {name: 'remove_pixel', icon: 'times'}
            ],
            activeTool: null,
            pixelTypes: [],
            activePixelType: null,
            availablePixels: {
                Value: null,
                MaxValue: null,
                AvailableOn: null,
            },
            ribbon_state: this.$store.state.ribbon_state,
            now: new Date(),
            sideNavOpened: false
        }
    }
}
