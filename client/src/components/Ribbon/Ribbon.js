import GameTab from "./GameTab/GameTab.vue";
import PlayerTab from "./PlayerTab/PlayerTab.vue";
import CityTab from "./CityTab/CityTab.vue";
import ClanTab from "./ClanTab/ClanTab.vue";
import WorldTab from "./WorldTab/WorldTab.vue";
import MarketTab from "./MarketTab/MarketTab.vue";

export default {
  name: 'Ribbon',
  components: { GameTab, PlayerTab, CityTab, ClanTab, WorldTab, MarketTab },
  mounted() {

  },
  methods: {
    selectTab(tab) {
      this.$router.push({name: tab.state});
    },
    isTabActive(tab) {
      return _.some(this.$router.currentRoute.matched, {name: tab.state});
    }
  },
  computed: {
    current_user () {
      return this.$store.state.current_user
    }
  },
  data() {
    return {
      tabs: [
        {state: 'game-page', name: 'game_tab'},
        {state: 'player-page', name: 'player_tab'},
        {state: 'city-page', name: 'city_tab'},
        {state: 'clan-page', name: 'clan_tab'},
        {state: 'world-page', name: 'world_tab'},
        {state: 'market-page', name: 'market_tab'}
      ]
    }
  }
}
