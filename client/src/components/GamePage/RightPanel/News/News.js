import {
  EventBus,
  EVENT_NEWS_WEBSOCKET_MESSAGE
} from '../../../../services/events';

import moment from 'moment';

export default {
  name: 'News',
  mounted() {
    EventBus.$on(EVENT_NEWS_WEBSOCKET_MESSAGE, (message_data) => {
      this.news_list.push(message_data.Data);
    });

    this.getNewsList();
    this.getLastUserNews();
  },
  methods: {
    getNewsList(){
      this.$http.get('news')
        .then((response) => {
          this.news_list = response.data;
        });
      return false;
    },
    sendNews(){
      if(!this.$store.state.current_user)
        return;

      this.$http.post(`news`, {Text: this.new_news})
        .then((response) => {
          this.new_news = '';
        });
    },
    getLastUserNews(){
      if(!this.current_user)
        return;

      this.$http.get('last_user_news')
        .then((response) => {
          if(!response)
            return;

          this.last_user_news = response.data;
        });
    }
  },
  computed: {
    current_user () {
      return this.$store.state.current_user
    },
    can_post_news () {
      return this.current_user && (this.current_user.IsAdmin ||
        (_.find(this.current_user.Permissions, {key: 'news'}) && (!this.last_user_news || this.hours_from_last_user_news >= 24)));
    },
    hours_from_last_user_news(){
      if(!this.last_user_news)
        return null;

      let duration = moment.duration(moment().diff(this.last_user_news));
      return duration.asHours();
    }
  },
  data() {
    return {
      loading: true,
      news_list: [],
      new_news: '',
      last_user_news: null
    }
  }
}
