import {
  EventBus,
  EVENT_CITY_WEBSOCKET_MESSAGE,
  EVENT_CITY_USER_WEBSOCKET_MESSAGE
} from '../../../../services/events';

import moment from 'moment';
import Locale from "../../../../services/locale";

export default {
  name: 'Cities',
  mounted() {
    this.getCities();

    EventBus.$on(EVENT_CITY_WEBSOCKET_MESSAGE, (message_data) => {
      this.getCities();
    });

    EventBus.$on(EVENT_CITY_USER_WEBSOCKET_MESSAGE, (message_data) => {
      this.getCities();
    });
  },
  methods: {
    getCities() {
      this.$http.get('cities')
        .then((response) => {
          this.cities = response.data;
          this.cities_loaded = true;
        }, () => this.cities_loaded = true);
      return false;
    },
    joinCity(city){
      if(!confirm('Are you sure to join the city "' + city.Name + '"?'))
        return;

      this.$http.post('city_join/' + city.ID)
        .then((response) => {
          this.$notify({
            type: 'success',
            title: Locale.get("success"),
            text: Locale.get("success_city_join")
          });
        });
    },
    selectCity(city){
      if(this.active_city && this.active_city.ID === city.ID)
        this.$store.commit('active_city', null);
      else
        this.$store.commit('active_city', city);
    }
  },
  computed: {
    current_user () {
      return this.$store.state.current_user
    },
    active_city () {
      return this.$store.state.active_city
    },
    hide_create_or_join_city() {
      return this.$store.state.hide_create_or_join_city
    }
  },
  data() {
    return {
      cities: [],
      cities_loaded: false
    }
  }
}
