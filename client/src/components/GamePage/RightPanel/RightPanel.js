export default {
    name: 'RightPanel',
    mounted() {

    },
    methods: {},
    computed: {
        current_user() {
            return this.$store.state.current_user
        },
        current_world() {
            return this.$store.state.current_world
        },
        current_user_loaded() {
            return this.$store.state.current_user_loaded
        },
        has_notification() {
            return this.$store.state.has_notification
        }
    },
    data() {
        return {}
    }
}
