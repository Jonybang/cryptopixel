import {
  EventBus,
  EVENT_CHAT_WEBSOCKET_MESSAGE,
  EVENT_CHAT_USER_WEBSOCKET_MESSAGE
} from '../../../../services/events';

import * as _ from 'lodash';

export default {
  name: 'Chat',
  mounted() {
    EventBus.$on(EVENT_CHAT_WEBSOCKET_MESSAGE, (message_data) => {
        if(this.active_chat.ID == message_data.Data.ChatID){
          this.active_chat.Messages.push(message_data.Data);
        }
    });

    EventBus.$on(EVENT_CHAT_USER_WEBSOCKET_MESSAGE, (message_data) => {
      if(this.current_user.ID == message_data.Data.UserID){
        this.getChatsList();
      }
    });

    this.getChatsList();
  },
  methods: {
    getChatsList(){
      this.$http.get('chats')
        .then((response) => {
          this.chats = response.data;
          this.active_chat = this.chats[0];
        });
      return false;
    },
    getChatMessages(chat){
      this.$http.get(`chat/${chat.ID}/messages`)
        .then((response) => {
          this.loading = false;

          chat.Messages = response.data;
        });
    },
    getChatMembers(chat){
      this.$http.get(`chat/${chat.ID}/members`)
        .then((response) => {
          chat.Members = response.data;
        });
    },
    sendNewMessage(){
      this.$http.post(`chat/${this.active_chat.ID}/messages`, {Text: this.new_message})
        .then((response) => {
          this.new_message = '';
        });
    },
    selectChat(ChatID){
      this.active_chat = _.find(this.chats, {ID: ChatID});
      this.getChatMessages(this.active_chat);
      this.getChatMembers(this.active_chat);
    }
  },
  computed: {
    current_user () {
      return this.$store.state.current_user
    }
  },
  data() {
    return {
      loading: true,
      chats: [],
      active_chat: {},
      new_message: ''
    }
  }
}
