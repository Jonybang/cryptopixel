import {
  EventBus,
  EVENT_USER_AUTHORIZED,
  EVENT_JOIN_LEAVE_WEBSOCKET_MESSAGE,
  EVENT_MAIN_TOOL_SELECTED
} from '../../../../services/events';

import {Helper as helper} from "../../../../services/helper";
import * as _ from 'lodash';

export default {
  name: 'RightPanelInfo',
  mounted() {
    this.getUsers();

    EventBus.$on(EVENT_JOIN_LEAVE_WEBSOCKET_MESSAGE, (message_data) => {
      this.getUsers();
    });
  },
  methods: {
    vkAuth(){
      helper.openWindow(this.$store.state.backend_url + 'vkontakte/auth', 'Авторизация вконтакте', ()=>{
        EventBus.$emit(EVENT_USER_AUTHORIZED);
      });
      return false;
    },
    getUsers(){
      this.$http.get('users')
        .then((response) => {
          this.users = response.data;
        });
    },
    createCity(){
      EventBus.$emit(EVENT_MAIN_TOOL_SELECTED, 'add_city');
    }
  },
  computed: {
    current_user () {
      return this.$store.state.current_user
    },
    current_world () {
      return this.$store.state.current_world
    },
    current_user_loaded () {
      return this.$store.state.current_user_loaded
    },
    hide_create_or_join_city() {
      return this.$store.state.hide_create_or_join_city
    },
    user_ref_link(){
      let uri = '/' + (this.current_world.Language === 'en' ? '' : this.current_world.Language + '/');
      return "http://cryptopixel.net" + uri + "?ref=" + this.current_user.RefLink;
    }
  },
  data() {
    return {
      loading: true,
      users: [],
      worlds: []
    }
  }
}
