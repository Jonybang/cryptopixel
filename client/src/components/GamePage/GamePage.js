import RightPanel from "./RightPanel/RightPanel.vue";
import Ribbon from "../Ribbon/Ribbon.vue";
import PixelArea from "./PixelArea/PixelArea.vue";

import {
  EventBus,
  EVENT_MAIN_TOOL_SELECTED
} from '../../services/events';

export default {
    name: 'game-page',
    components: {
        'right-panel': RightPanel,
        'ribbon': Ribbon,
        'pixel-area': PixelArea
    },
    created() {
      this.$store.watch((state) => state.current_user, this.notAuthNotification.bind(this));
      this.$store.watch((state) => state.current_user && state.current_user.CityID, this.notAuthNotification.bind(this));
      this.$store.watch((state) => state.current_user_loaded, this.notAuthNotification.bind(this));

      this.notAuthNotification();

      EventBus.$on(EVENT_MAIN_TOOL_SELECTED, (tool_name) => {
        this.sideNavOpened = false;
      });
    },
    mounted() {
    },
    methods: {
        notAuthNotification() {
            if (!this.$store.state.current_user_loaded)
                return;

            if (!this.$store.state.current_user)
                return this.$store.commit('has_notification', true);

            if (!this.$store.state.current_user.CityID)
                return this.$store.commit('has_notification', true);

            return this.$store.commit('has_notification', false);
        }
    },
    data() {
        return {
            sideNavOpened: false
        }
    },
    computed: {
        current_world() {
            return this.$store.state.current_world
        },
        has_notification() {
            return this.$store.state.has_notification
        }
    },
}
