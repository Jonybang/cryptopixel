import * as _ from 'lodash';
import {AbstractDrawing} from "../../../libs/AbastractDrawing";
import ProjectLocalStorage from "../../../services/projectLocalStorage";

export class PixelDrawing extends AbstractDrawing {
  options = null;
  pixelType = null;

  areaPosition = {x: 0, y: 0};
  areaSize = {width: 0, height: 0};

  subData = [];

  currentEntity = null;
  activeCity = null;

  coordinatesPopover = null;

  blocked = false;

  storage = null;

  pixelsGroup = null;
  rasterPixelGroup = null;

  constructor(canvasEl, options) {
    super(canvasEl);

    this.options = options;

    this.storage = new ProjectLocalStorage('PixelDrawing');

    if(this.storage.get('view.zoom'))
      this.view.zoom = parseFloat(this.storage.get('view.zoom'));

    this.paper.settings.handleSize = 10;
    this.paper.settings.hitTolerance = 8;

    this.resizeArea();

    if(this.storage.get('view.center')){
      let center = this.storage.get('view.center');
      this.view.center = new this.paper.Point(center);
      console.log('storage center', center);
    }

    // this.view.size.width = options.max_x;
    // this.view.size.height = options.max_y;
  }

  resizeArea(){
    this.areaSize.width = this.canvasEl.parentElement.offsetWidth;
    this.canvasEl.setAttribute('width', this.areaSize.width);
    this.canvasEl.style.setProperty('width', this.areaSize.width + 'px');

    this.view.viewSize.width = this.areaSize.width;

    this.areaSize.height = this.canvasEl.parentElement.offsetHeight;
    this.canvasEl.setAttribute('height', this.areaSize.height);
    this.canvasEl.style.setProperty('height', this.areaSize.height + 'px');

    this.view.viewSize.height = this.areaSize.height;
  }

  initArea(pixels) {
    this.removeCoordinatesPopover();

    this.paper.project.clear();
    this.initBackground();

    if (!pixels || !pixels.length)
      return;

    if(this.rasterPixelGroup){
      this.rasterPixelGroup.remove();
    }

    this.pixelsGroup = new this.paper.Group();

    _.each(pixels, (pixel) => {
      this.initPixel(pixel, true);
    });

    this.rasterPixelGroup = this.pixelsGroup.rasterize(300);
    this.rasterPixelGroup.insertAbove(this.worldBackground);

    this.removePixelGroup();
  }

  removePixelGroup(){
    if(this.pixelsGroup) {
      _.forEach(this.pixelsGroup.children, (child) => {
        if(child)
          child.remove()
      });
      this.pixelsGroup.remove();
      this.pixelsGroup = null;
    }
  }

  initSubEntities(data) {
    this.subData = data;
  }

  initPixel(pixel, withBackground = false, bellowThePixel = false) {
    let fillColor;

    if(this.activeCity && pixel.CityID === this.activeCity.ID && pixel.IsCityEdge)
      fillColor = 'blue';
    else
      fillColor = pixel.Type.Color;

    let object = new this.paper.Shape.Rectangle({
      point: [pixel.X, pixel.Y],
      size: 5,
      fillColor: fillColor
    });

    object.bringToFront();

    if(this.pixelsGroup)
      this.pixelsGroup.addChild(object);

    if(withBackground){

      let background_object = new this.paper.Shape.Rectangle({
        point: [pixel.X, pixel.Y],
        size: 5,
        strokeColor: fillColor,
        strokeWidth: 1,
        strokeScaling: false
      });

      if(bellowThePixel){
        background_object.bringToFront();
        background_object.insertBelow(object);
      } else {
        background_object.insertAbove(this.worldBackground);
      }
      object.data.background = background_object;
      if(this.pixelsGroup)
        this.pixelsGroup.addChild(background_object);
    }

    return object;
  }

  placeEntity(point, data, callback) {
    //TODO: get pixel size from api
    point.x -= point.x % 5;
    point.y -= point.y % 5;

    let object = {
      X: point.x,
      Y: point.y
    };

    if(data)
      _.extend(object, data);

    if(callback){
      callback.bind(this);
      callback(object);
    }

    return object;
  }

  draw() {
    this.paper.view.draw();
  }

  drawCoordinatesPopover(point){
    let rectanglePoint = [point.x, point.y - 50];
    let pixelX = point.x -= point.x % 5;
    let pixelY = point.y -= point.y % 5;
    let textContent = pixelX + ',' + pixelY;

    if(this.coordinatesPopover) {
      this.coordinatesPopover.bringToFront();
      this.coordinatesPopover.position = rectanglePoint;
      let popoverData = this.coordinatesPopover.data;
      popoverData.text.position = rectanglePoint;
      popoverData.text.content = textContent;
      popoverData.text.bringToFront();
      popoverData.pixel.bounds.topLeft = {x: pixelX, y: pixelY};

      if(popoverData.pixel.data.background){
        popoverData.pixel.data.background.bounds.topLeft = {x: pixelX, y: pixelY};
        popoverData.pixel.data.background.bringToFront();
      }

      popoverData.pixel.bringToFront();
      return;
    }

    this.coordinatesPopover = new this.paper.Shape.Rectangle({
      point: rectanglePoint,
      size: [80, 25],
      fillColor: 'black',
      strokeColor: 'white',
      strokeWidth: 1,
      strokeScaling: false
    });

    let text = new this.paper.PointText(rectanglePoint);
    text.justification = 'center';
    text.fillColor = 'white';
    text.content = textContent;

    this.coordinatesPopover.bringToFront();
    this.coordinatesPopover.position = rectanglePoint;

    let popoverData = this.coordinatesPopover.data;

    popoverData.text = text;
    popoverData.text.position = rectanglePoint;
    popoverData.text.content = textContent;
    popoverData.text.bringToFront();

    popoverData.pixel = this.initPixel({X: pixelX, Y: pixelY, Type: this.pixelType});
    popoverData.pixel.bounds.topLeft = {x: pixelX, y: pixelY};
    if(popoverData.pixel.data.background){
      popoverData.pixel.data.background.bounds.topLeft = {x: pixelX, y: pixelY};
      popoverData.pixel.data.background.bringToFront();
    }
    popoverData.pixel.bringToFront();
  }
  removeCoordinatesPopover(){
    if(this.coordinatesPopover)
      this.coordinatesPopover.remove();
    else
      return;

    if(this.coordinatesPopover && this.coordinatesPopover.data.text)
      this.coordinatesPopover.data.text.remove();

    if(this.coordinatesPopover && this.coordinatesPopover.data.pixel)
      this.coordinatesPopover.data.pixel.remove();

    if(this.coordinatesPopover && this.coordinatesPopover.data.pixel && this.coordinatesPopover.data.pixel.data.background)
      this.coordinatesPopover.data.pixel.data.background.remove();

    this.coordinatesPopover = null;
  }
  onMouseMove(event) {
    if(this.blocked)
      return;

    switch (this.tool) {
      case 'add_city':
        if(!this.currentEntity){
          this.currentEntity = new this.paper.Path.Circle([event.point.x, event.point.y], 125);
          this.currentEntity.strokeColor = 'blue';
          this.currentEntity.strokeWidth = 2;
        }

        this.currentEntity.bounds.center.x = event.point.x;
        this.currentEntity.bounds.center.y = event.point.y;
        break;
      case 'add_pixel':
        this.drawCoordinatesPopover(event.point);
        break;
    }
  }

  onMouseDown(event) {
    if(this.blocked)
      return;

    if(event.event.button === 2){
      //right click
      return;
    }

    this.bDragging = true;

    switch (this.tool){
      case 'move':
        this.dragViewPoint = this.view.projectToView(event.point);
        break;
      case 'add_pixel':
        this.drawCoordinatesPopover(event.point);
        break;
    }
  }

  onMouseDrag(event) {
    if(this.blocked)
      return;

    if(event.event.button === 2){
      //right click
      return;
    }

    if (!this.bDragging)
      return;

    switch (this.tool){
      case 'move':
        let point = this.view.projectToView(event.point),
          last = this.view.viewToProject(this.dragViewPoint);
        this.dragViewPoint = point;
        this.view.scrollBy(last.subtract(event.point));
        this.saveViewCenter();

        this.handleViewDrag();
        break;
      case 'add_pixel':
        this.drawCoordinatesPopover(event.point);
        break;
    }
  }

  handleViewDrag(){
    this.initBackground();

    if(this.onViewDrag)
      this.onViewDrag();
  }

  initBackground(){
    if(this.worldBackground)
      this.worldBackground.remove();

    this.worldBackground = new this.paper.Shape.Rectangle({
      point: [0, 0],
      size: [this.options.max_x, this.options.max_y],
      fillColor: 'white'
    });
    this.worldBackground.sendToBack();

    if(this.canvasBackground)
      this.canvasBackground.remove();

    this.canvasBackground = new this.paper.Shape.Rectangle({
      point: [this.view.bounds.topLeft.x, this.view.bounds.topLeft.y],
      size: [this.view.bounds.width, this.view.bounds.height],
      fillColor: 'grey'
    });
    this.canvasBackground.sendToBack();
  }

  onMouseUp(event) {
    if(this.blocked)
      return;

    if(event.event.button === 2){
      //right click
      return;
    }
    // if (!this.bDragging)
    //   return;

    this.bDragging = false;

    switch (this.tool) {
      case 'add_pixel':
      case 'remove_pixel':
        this.placeEntity(event.point, {TypeID: this.pixelType.ID}, this.onAddPixel);
        break;
      case 'add_city':
        if(!this.currentEntity) {
          this.onMouseMove(event);
        }
        this.placeEntity(this.currentEntity.bounds.center, {}, this.onAddCity);
        this.removeCoordinatesPopover();
        break;
    }
    if(this.currentEntity){
      this.currentEntity.remove();
      this.currentEntity = null;
    }
  }

  onKeyDown(event) {
    if(this.blocked)
      return;

    switch (event.key) {
      case '=':
        if (this.view.zoom >= 1)
          this.view.zoom += 0.1;
        else
          this.view.zoom *= 1.1;
        this.storage.set('view.zoom', this.view.zoom);
        break;
      case '-':
        if (this.view.zoom >= 1)
          this.view.zoom -= 0.1;
        else
          this.view.zoom /= 1.1;
        this.storage.set('view.zoom', this.view.zoom);
        this.handleViewDrag();
        break;
      case 'delete':
        break;
      case 'enter':
        break;
    }
  }

  actionEnd() {
    if(this.currentEntity){
      this.currentEntity.remove();
      this.currentEntity = null;
    }
  }

  drawWeb(param, scale) {
    super.drawWeb(param, scale);

    this.image.sendToBack();
  }

  saveViewCenter(){
    let center = _.pick(this.view.center, ['x', 'y']);
    this.storage.set('view.center', center);
  }
}
