import {ModalItem} from '../../../../directives/AsyncModal'
import * as _ from 'lodash';

export default {
  components: {
    ModalItem
  },
  created(){
    if (this.current_user && this.current_user.ClanID) {
      this.city.Clan = this.current_user.Clan;
    }
  },
  methods: {
    ok () {
      this.$store.commit('create_city_modal', this.city);
      this.$root.$asyncModal.close('create-city-modal');
    },
    cancel () {
      this.$store.commit('create_city_modal', null);
      this.$root.$asyncModal.close('create-city-modal');
    },
    validateCity: _.debounce(function () {
      this.$http.post('city_validate', this.city).then((response) => {
        this.city_state.valid = response.data.Valid;
        this.city_state.error_message = response.data.Message;
        this.city_state.loading = false;
      })
    }, 500),
    validateClan: _.debounce(function () {
      this.$http.post('clan_validate', this.city.Clan).then((response) => {
        this.clan_state.valid = response.data.Valid;
        this.clan_state.error_message = response.data.Message;
        this.clan_state.loading = false;
      })
    }, 500)
  },
  watch: {
    'city.Name': function (newCity, oldCity) {
      this.city_state.loading = true;
      this.validateCity();
    },
    'city.Clan.Name': function (newCity, oldCity) {
      this.clan_state.loading = true;
      this.validateClan();
    }
  },
  data: function () {
    return {
      city: {
        Name: "",
        Clan: {}
      },
      city_state: {
        valid: true,
        loading: false,
        error_message: ""
      },
      clan_state: {
        valid: true,
        loading: false,
        error_message: ""
      }
    }
  },
  computed: {
    current_user () {
      return this.$store.state.current_user
    },
    invalid(){
      return this.city_state.loading || this.clan_state.loading
        || !this.clan_state.valid || !this.city_state.valid
        || !this.city.Name || !(this.city.Clan.ID || this.city.Clan.Name);
    }
  }
}
