import {
  EventBus,
  EVENT_MAIN_TOOL_UNSELECTED,
  EVENT_MAIN_TOOL_SELECTED,
  EVENT_MAIN_PIXEL_TYPE_SELECTED,
  EVENT_PIXEL_WEBSOCKET_MESSAGE,
  EVENT_USER_AUTHORIZED,
  EVENT_CITY_WEBSOCKET_MESSAGE,
  EVENT_MAIN_TOOL_RESET,
  EVENT_JOIN_LEAVE_WEBSOCKET_MESSAGE
} from '../../../services/events';

import * as _ from 'lodash';
import {PixelDrawing} from "./PixelDrawing";
import CreateCityModal from "./CreateCityModal/CreateCityModal.vue";
import Locale from "../../../services/locale";

export default {
  name: 'pixel-area',
  created() {
    EventBus.$on(EVENT_MAIN_TOOL_SELECTED, (toolName) => {
      this.pixel_drawing.tool = this.currentTool = toolName;
      this.pixel_drawing.actionEnd();
    });
    EventBus.$on(EVENT_MAIN_TOOL_UNSELECTED, () => {
      this.pixel_drawing.tool = this.currentTool = null;
      this.pixel_drawing.actionEnd();
    });
    EventBus.$on(EVENT_MAIN_PIXEL_TYPE_SELECTED, (pixelType) => {
      this.pixel_drawing.pixelType = this.pixelType = pixelType;
      this.pixel_drawing.actionEnd();
    });
  },
  mounted() {
    this.pixel_drawing = new PixelDrawing(this.$refs.gameCanvas, {
      max_x: this.current_world.MaxX,
      max_y: this.current_world.MaxY,
    });

    this.getPixels();

    EventBus.$on(EVENT_USER_AUTHORIZED, () => {
      this.getPixels();
    });

    EventBus.$on(EVENT_JOIN_LEAVE_WEBSOCKET_MESSAGE, (message_data) => {
      if(message_data.Type !== "join")
        return;

      if(this.current_user_loaded || (this.current_user && message_data.User === this.current_user.ID))
        this.getPixels();
    });

    window.onresize = _.debounce(() => {
      this.pixel_drawing.resizeArea();
      this.getPixels();
    }, 100);

    ['onMouseMove', 'onMouseDown', 'onMouseDrag', 'onMouseUp', 'onKeyDown'].forEach((eventName) => {
      this.pixel_drawing.paper.tool[eventName] = (event) => {
        this.pixel_drawing[eventName](event);
        this.pixel_drawing.draw();
      };
    });

    this.pixel_drawing.onViewDrag = _.debounce(() => {
      this.getPixels();
    }, 500);

    this.pixel_drawing.onAddPixel = (pixel) => {
      this.$http.post("pixels", pixel);
    };

    this.pixel_drawing.onAddCity = (city) => {

      EventBus.$emit(EVENT_MAIN_TOOL_RESET);

      this.$root.$asyncModal.open({
        id: 'create-city-modal',
        component: CreateCityModal,
        options: {
          destroy: true
        },
        onClose: (data) => {
          const create_city_modal = this.$store.state.create_city_modal;
          if(!create_city_modal)
            return;

          this.$http.post("cities", _.extend({}, create_city_modal, city)).then((response)=> {
            if(response.status === 200){
              this.$notify({
                title: Locale.get('please_wait'),
                text: Locale.get('city_create_request_sent')
              });
            }
            this.$store.commit('hide_create_or_join_city', true);
          }, () => this.$store.commit('hide_create_or_join_city', true));
        }
      });

    };

    EventBus.$on(EVENT_PIXEL_WEBSOCKET_MESSAGE, (message_data) => {
      if(message_data.Type !== "add_pixel")
        return;

      if(message_data.Data)
        this.pixel_drawing.initPixel(message_data.Data, true, true)
    });

    EventBus.$on(EVENT_CITY_WEBSOCKET_MESSAGE, (message_data) => {
      if(message_data.Type !== "add_city")
        return;

      let city = message_data.Data;

      this.$store.commit('hide_create_or_join_city', false);

      if(city.IsCreated){
        this.$notify({
          type: 'success',
          title: Locale.get('new_city'),
          text: Locale.get('city') + ' "' + city.Name + '" ' + Locale.get('has_created')
        });

        this.getPixels();
      }
    });

    this.$store.watch(
      // When the returned result changes...
      (state) => {
        return state.active_city
      },
      // Run this callback
      (city) => {
        if(city){
          this.pixel_drawing.view.center = { x: city.X, y: city.Y };
          this.pixel_drawing.saveViewCenter();
        }
        this.pixel_drawing.activeCity = city;
        this.getPixels();
      }
    )
  },
  watch: {

  },
  methods: {
    drawScaleWeb(){
      this.pixel_drawing.clearWeb();
      this.pixel_drawing.drawWeb('width', 2);
      this.pixel_drawing.drawWeb('height', 2);
      this.pixel_drawing.draw();
    },
    getPixels(){
      const view = this.pixel_drawing.view;
      const area = {
        StartX: Math.floor(view.bounds.topLeft.x),
        StartY: Math.floor(view.bounds.topLeft.y),
        EndX: Math.ceil(view.bounds.topLeft.x + this.pixel_drawing.areaSize.width / view.zoom),
        EndY: Math.ceil(view.bounds.topLeft.y + this.pixel_drawing.areaSize.height / view.zoom)
      };

      let pixels_promise = this.$http.get('pixels', area).then((response) => {
        this.pixel_drawing.initArea(response.data);
      });

      if(this.$root.$websocket.OPEN){
        setTimeout(sendAreaToSocket.bind(this), 500);
      } else {
        this.$root.$websocket.onopen = () => {
          setTimeout(sendAreaToSocket.bind(this), 500);
        }
      }

      function sendAreaToSocket() {
        try {
          this.$root.$websocket.send(JSON.stringify(area));
          console.log('websocket area sended!', area);
        } catch(e) {
          console.log('websocket resend area..');
          setTimeout(sendAreaToSocket.bind(this), 500);
        }
      }
    }
  },
  data () {
    return {
      currentTool: null
    }
  },
  computed: {
    ribbon_state () {
      return this.$store.state.ribbon_state
    },
    current_world () {
      return this.$store.state.current_world
    },
    current_user () {
      return this.$store.state.current_user
    },
    active_city () {
      return this.$store.state.active_city
    },
    current_user_loaded() {
      return this.$store.state.current_user_loaded
    }
  },
}
