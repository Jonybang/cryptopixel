import Vue from 'vue';

export const EventBus = new Vue();

export const EVENT_MAIN_TOOL_SELECTED = 'main-tool-selected';
export const EVENT_MAIN_TOOL_UNSELECTED = 'main-tool-unselected';
export const EVENT_MAIN_TOOL_RESET = 'main-tool-reset';
export const EVENT_MAIN_PIXEL_TYPE_SELECTED = 'main-pixel-type-selected';
export const EVENT_USER_AUTHORIZED = 'user-authorized';
export const EVENT_PIXEL_WEBSOCKET_MESSAGE = 'pixel-websocket-message';
export const EVENT_CHAT_WEBSOCKET_MESSAGE = 'chat-websocket-message';
export const EVENT_NEWS_WEBSOCKET_MESSAGE = 'news-websocket-message';
export const EVENT_JOIN_LEAVE_WEBSOCKET_MESSAGE = 'join-leave-websocket-message';
export const EVENT_AVAILABLE_ACTION_WEBSOCKET_MESSAGE = 'available-action-websocket-message';
export const EVENT_CITY_WEBSOCKET_MESSAGE = 'city-websocket-message';
export const EVENT_CITY_USER_WEBSOCKET_MESSAGE = 'city-user-websocket-message';
export const EVENT_CHAT_USER_WEBSOCKET_MESSAGE = 'chat-user-websocket-message';
export const EVENT_PROGRESS_USER_WEBSOCKET_MESSAGE = 'progress-user-websocket-message';
