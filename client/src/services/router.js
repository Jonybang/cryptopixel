import Vue from 'vue'
import Router from 'vue-router'
import GamePage from '../components/GamePage/GamePage.vue'
import PlayerPage from '../components/PlayerPage/PlayerPage.vue'
import CityPage from '../components/CityPage/CityPage.vue'
import CityUsers from '../components/CityPage/CityUsers/CityUsers.vue'
import CityLogs from '../components/CityPage/CityLogs/CityLogs.vue'
import ClanPage from '../components/ClanPage/ClanPage.vue'
import WorldPage from '../components/WorldPage/WorldPage.vue'
import WorldLogs from '../components/WorldPage/WorldLogs/WorldLogs.vue'
import WorldSettings from '../components/WorldPage/WorldSettings/WorldSettings.vue'
import MarketPage from '../components/MarketPage/MarketPage.vue'
import WorldsChoose from "../components/WorldsChoose/WorldsChoose.vue";
import RightPanelInfo from "../components/GamePage/RightPanel/Info/RightPanelInfo.vue";
import Chat from "../components/GamePage/RightPanel/Chat/Chat.vue";
import Cities from "../components/GamePage/RightPanel/Cities/Cities.vue";
import News from "../components/GamePage/RightPanel/News/News.vue";

Vue.use(Router);

export default new Router({
  //mode: 'history',
  routes: [
    {
      path: '/game',
      name: 'game-page',
      component: GamePage,
      children: [
        {
          path: 'info',
          name: 'game-info',
          component: RightPanelInfo,
        },
        {
          path: 'chat',
          name: 'game-chat',
          component: Chat,
        },
        {
          path: 'cities',
          name: 'game-cities',
          component: Cities,
        },
        {
          path: 'news',
          name: 'game-news',
          component: News,
        }
      ]
    },
    {
      path: '/player',
      name: 'player-page',
      component: PlayerPage
    },
    {
      path: '/city',
      name: 'city-page',
      component: CityPage,
      children: [
        {
          path: 'users',
          name: 'city-users',
          component: CityUsers,
        },
        {
          path: 'logs',
          name: 'city-logs',
          component: CityLogs,
        },
        {
          path: '*', redirect: 'users'
        },
      ]
    },
    {
      path: '/clan',
      name: 'clan-page',
      component: ClanPage
    },
    {
        path: '/world',
        name: 'world-page',
        component: WorldPage,
        children: [
          {
            path: 'logs',
            name: 'world-logs',
            component: WorldLogs,
          },
          {
            path: 'settings',
            name: 'world-settings',
            component: WorldSettings,
          },
          {
            path: '*', redirect: 'logs'
          },
        ]
    },
    {
        path: '/market',
        name: 'market-page',
        component: MarketPage
    },
    {
      path: '/worlds',
      name: 'worlds',
      component: WorldsChoose
    },
    {
      path: '*', redirect: 'game/info'
    }
  ]
})
