import Vue from 'vue';
import Vuex from 'vuex';
import ProjectLocalStorage from "./projectLocalStorage";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    ribbon_state: null,
    current_user: null,
    current_user_loaded: false,
    current_world: null,
    active_city: null,
    create_city_modal: null,
    has_notification: false,
    hide_create_or_join_city: false,
    backend_url: process.env.NODE_ENV === 'development' ? 'http://localhost:8090/' : '/'
  },
  mutations: {
    ribbon_state (state, ribbon_state) {
      state.ribbon_state = ribbon_state;
    },
    current_user (state, current_user) {
      state.current_user = current_user;
    },
    current_user_loaded (state, current_user_loaded) {
      state.current_user_loaded = current_user_loaded;
    },
    current_world (state, current_world) {
      ProjectLocalStorage.initWorld(current_world);

      state.current_world = current_world;
    },
    active_city (state, active_city) {
      state.active_city = active_city;
    },
    create_city_modal(state, create_city_modal){
      state.create_city_modal = create_city_modal;
    },
    has_notification(state, has_notification){
      state.has_notification = has_notification;
    },
    hide_create_or_join_city(state, hide_create_or_join_city){
      state.hide_create_or_join_city = hide_create_or_join_city;
    }
  }
});
