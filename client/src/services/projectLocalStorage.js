import * as _ from 'lodash';

export default class ProjectLocalStorage {
    static worldId;
    static nameSpace;

    constructor(nameSpace){
      this.nameSpace = nameSpace;
    }

    get(key) {
        let value = localStorage.getItem(this.nameSpace + '.' + key);
        try {
          return JSON.parse(value);
        } catch (e) {
          return value;
        }
    }
    set(key, value) {
      if(_.isObject(value))
        value = JSON.stringify(value);

      localStorage.setItem(this.nameSpace + '.' + key, value);
    }

    static initWorld(world){
      ProjectLocalStorage.worldId = world.ID;
    }
}
