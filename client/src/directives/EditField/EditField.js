import * as _ from 'lodash';

export default {
  name: 'edit-field',
  props: [ 'value', 'state', 'readonly' ],
  watch: {
    value: function () {
      this.changed();
    }
  },
  methods: {
    changed(value){
      if(_.isUndefined(value))
        return;

      this.$emit('input', value);
    }
  },
  template: `<span>
      <input v-if="!readonly" type="text" :class="{'form-control': true, 'invalid':!state.valid}" v-bind:value="value" v-on:input="changed($event.target.value)">
      <span v-if="readonly">{{value}}</span>
      <loader v-if="state.loading"></loader>
      <span class="error">{{state.error_message}}</span>
    </span>
  `
}
