import * as _ from 'lodash';
import * as paper from 'paper';

export class AbstractDrawing {

  sStrokeNormal = '#00ffff';
  sStrokeHighlight = '#00ff00';
  sStrokeDrawing = '#cccccc';
  sFillNormal = 'rgba(0, 255, 255, 0.1)';
  sFillHighlight = 'rgba(0, 255, 0, 0.1)';
  sFillDrawing = 'rgba(255, 255, 255, 0.15)';

  nPointRadius = 5;
  nStrokeWidth = 5;

  webElements = [];

  canvasEl = null;

  constructor(canvasEl) {
    this.canvasEl = canvasEl;
    this.paper = new paper.PaperScope();
    this.paper.tool = new this.paper.Tool();

    this.paper.setup(canvasEl);

    this.view = this.paper.project.view;
  }

  drawWeb(param, scale, options) {
    const viewParamValue = this.view.size[param];

    const baseStep = options && options.baseStep ? options.baseStep : 50;

    let step = baseStep + baseStep / 2;
    let stepValue = scale * step;
    while (stepValue % baseStep != 0) {
      if (stepValue % baseStep < baseStep / 2)
        step += scale > 1 ? -0.1 : 0.1;
      else
        step += scale > 1 ? 0.1 : 0.1;
      stepValue = parseInt(scale * step);
    }

    if (!step)
      return console.log('0 step');

    let currentValue = step;
    while (currentValue < viewParamValue) {
      let meterValue = parseInt(scale * currentValue);

      while (meterValue % baseStep != 0) {
        if (meterValue % baseStep < baseStep / 2)
          currentValue += scale > 1 ? -0.1 : 0.1;
        else
          currentValue += scale > 1 ? 0.1 : 0.1;
        meterValue = parseInt(scale * currentValue);
      }

      let line = new this.paper.Path();
      line.strokeWidth = 1;
      line.strokeColor = 'red';

      let pixelValue = options && options.reverse ? viewParamValue - currentValue : currentValue;

      if (param == 'width') {
        line.add(new this.paper.Point({x: pixelValue, y: 0}));
        line.add(new this.paper.Point({x: pixelValue, y: this.view.size.height}));
      } else if (param == 'height') {
        line.add(new this.paper.Point({x: 0, y: pixelValue}));
        line.add(new this.paper.Point({x: this.view.size.width, y: pixelValue}));
      }

      line.sendToBack();
      this.webElements.push(line);

      let line_text = new this.paper.PointText({
        point: param == 'width' ? {x: pixelValue + 5, y: 15} : {x: 5, y: pixelValue + 15},
        fillColor: 'red',
        content: meterValue
      });
      line_text.sendToBack();

      this.webElements.push(line_text);

      currentValue += step;
    }
  }

  clearWeb() {
    _.forEach(this.webElements, (webEl) => {
      webEl.remove();
    });
    this.webElements = [];
  }
}
