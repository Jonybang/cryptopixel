Init go environment:
echo "export GOPATH=~/go" >> ~/.profile

Create new golang app:
dep init

Install dependencies:
dep ensure

Add package:
dep ensure -add github.com/jinzhu/gorm

Create database:
mysql -u root -p -e "CREATE USER 'geek_pixel'@'localhost' IDENTIFIED BY 'Jb192837'; create database geek_pixel DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci; GRANT ALL PRIVILEGES ON geek_pixel . * TO 'geek_pixel'@'localhost';"

Re-create database:
mysql -u root -p -e "drop user geek_pixel@'localhost'; CREATE USER 'geek_pixel'@'localhost' IDENTIFIED BY 'Jb192837'; drop database geek_pixel; create database geek_pixel DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci; GRANT ALL PRIVILEGES ON geek_pixel . * TO 'geek_pixel'@'localhost';"

Build:
go build server.go && ./server

Build frontend:
cd client && npm run start

Packages to will use:
https://github.com/ByteArena/box2d

MVC:
https://github.com/gernest/utron

Templates:
https://golang.org/pkg/text/template/

Websockets sample:
https://github.com/utronframework/chat

ORM:
http://jinzhu.me/gorm/
