package main
// Thanks @qwertmax for this example
// (http://stackoverflow.com/questions/29359907/social-network-vk-auth-with-martini)


import (
	"log"
	"github.com/gernest/utron"
	"fmt"
	"net/http"
	"github.com/srinathgs/mysqlstore"
	_"github.com/gernest/utron/app"
	"github.com/rs/cors"
	"geek-pixel/backend/app_models"
	"geek-pixel/backend/app_controllers"
	"geek-pixel/backend/app_controllers/api"
	"geek-pixel/backend/seeds"
	"github.com/robfig/cron"
	"time"
	"geek-pixel/backend/pixel_room"
	"github.com/gernest/utron/app"
	"encoding/gob"
	"geek-pixel/backend/helpers"
	"strconv"
	"github.com/jinzhu/gorm"
)

func main() {
	app, err := utron.NewMVC()
	if err != nil {
		log.Fatal(err)
	}

	gob.Register(helpers.AreaStruct{})

	config := helpers.GetConfig()

	app.SessionStore, err = mysqlstore.NewMySQLStore(config.DatabaseConn, "sessions", "/", 604800, []byte("TakenAdviceSpoTakenAdviceSpo2222"))
	if err != nil {
		panic(err)
	}
	app.Router.Options.SessionStore = app.SessionStore

	app.Model.Register(&app_models.Setting{})

	app.Model.Register(&app_models.World{})
	app.Model.Register(&app_models.Clan{})
	app.Model.Register(&app_models.City{})

	app.Model.Register(&app_models.User{})
	app.Model.Register(&app_models.UserSocial{})
	app.Model.Register(&app_models.UserProgress{})
	app.Model.Register(&app_models.Wallet{})
	app.Model.Register(&app_models.Permission{})
	app.Model.Register(&app_models.Role{})
	app.Model.Register(&app_models.PermissionRole{})
	app.Model.Register(&app_models.UserRole{})
	app.Model.Register(&app_models.UserLog{})
	app.Model.Register(&app_models.AvailableUserAction{})

	app.Model.Register(&app_models.PixelType{})
	app.Model.Register(&app_models.Pixel{})
	app.Model.Register(&app_models.PixelEffectType{})
	app.Model.Register(&app_models.PixelEffect{})

	app.Model.Register(&app_models.Chat{})
	app.Model.Register(&app_models.ChatMember{})
	app.Model.Register(&app_models.ChatMessage{})

	app.Model.Register(&app_models.NewsFeed{})

	app.Model.AutoMigrateAll()

	seeds.Run(app.Model.DB, true)
	//app.Model.DB.LogMode(true)

	app.Model.DB.Model(&app_models.User{}).Where("is_online = ?", true).Update(map[string]interface{}{"is_online": false, "last_online_at": time.Now()})

	cities := []app_models.City{}
	app.Model.DB.Find(&cities)

	for index, city := range cities {
		_ = index
		city.CacheUsersCount(app.Model.DB)
	}

	app.AddController(app_controllers.NewMain)
	app.AddController(app_controllers.NewVkontakte)
	app.AddController(app_controllers.NewWebsocket)

	app.AddController(api.NewChats)
	app.AddController(api.NewPixels)
	app.AddController(api.NewUsers)
	app.AddController(api.NewRoles)
	app.AddController(api.NewNews)
	app.AddController(api.NewWorlds)
	app.AddController(api.NewClans)
	app.AddController(api.NewCities)

	log.Print("Running cron...")

	run_cron(app);

	port := fmt.Sprintf(":%d", app.Config.Port)
	log.Print("App running on port " + port)

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:8080"},
		AllowCredentials: true,
	})
	handler := c.Handler(app)
	log.Fatal(http.ListenAndServe(port, handler))
}

func run_cron(app *app.App){
	db := app.Model.DB
	app_cron := cron.New()
	process_available_actions_complete := true
	cron_err := app_cron.AddFunc("@every 10s", func() {
		if(!process_available_actions_complete){
			return;
		}

		process_available_actions_complete = false

		var available_actions []app_models.AvailableUserAction
		db.Where("is_auto = ?", true).Where("max_value > value").Where("available_on <= ?", time.Now()).Preload("City").Preload("Clan").Preload("Chat").Find(&available_actions)

		for index, action := range available_actions {
			_ = index
			action.Value += action.IncrementValue

			if(action.MaxValue > action.Value){
				available_on := time.Now().Add(time.Second * time.Duration(action.SecondsPeriod))
				action.AvailableOn = &available_on
			} else {
				action.AvailableOn = nil
			}

			db.Save(&action)
			pixel_room.ChangeAvailableAction(int(action.UserID), action)
		}
		process_available_actions_complete = true
	})

	if(cron_err != nil){
		fmt.Println("cron_err: " + cron_err.Error())
	}

	process_inactive_users_complete := true
	cron_err = app_cron.AddFunc("@every 1m", func() {
		if(!process_inactive_users_complete){
			return;
		}

		process_inactive_users_complete = false

		var worlds_inactive_periods []app_models.Setting
		db.Find(&worlds_inactive_periods, "`key` = ?", "inactive_seconds_period")

		for index, period := range worlds_inactive_periods {
			_ = index

			int_duration, err := strconv.Atoi(period.Value)
			if(err == nil){
				db.Model(app_models.User{}).Where("is_online = false AND is_inactive = false AND last_online_at < ?", time.Now().Add(- time.Second * time.Duration(int_duration))).Update("is_inactive", true)
			} else {
				log.Println("[Error] Incorect inactive_seconds_period: ", period.Value)
			}
		}

		process_inactive_users_complete = true
	})


	var worlds_inactive_penalty_cron []app_models.Setting
	db.Find(&worlds_inactive_penalty_cron, "`key` = ?", "inactive_penalty_cron")

	for index, cron_setting := range worlds_inactive_penalty_cron {
		_ = index
		cron_func := InitInactivePenaltyCron(db, cron_setting)
		cron_err = app_cron.AddFunc(cron_setting.Value, cron_func)
	}


	if(cron_err != nil){
		fmt.Println("cron_err: " + cron_err.Error())
	}

	app_cron.Start()
}

func InitInactivePenaltyCron (db *gorm.DB, cron_setting app_models.Setting) func() {

	return func(){
		rows, err := db.Table("cities").Where("world_id = ?", cron_setting.WorldID).Select("id, owner_id, (?) as inactive_users_count", db.Table("users").Select("COUNT(*)").Where("city_id = cities.id AND is_inactive = true").QueryExpr()).Rows()

		if err != nil {
			log.Fatal(err)
		}

		var emptyPixelType  = app_models.PixelType{}
		db.First(&emptyPixelType, "`key` = ?", "grey")

		for rows.Next() {
			var city_id int
			var owner_id int
			var inactive_users_count int

			err := rows.Scan(&city_id, &owner_id, &inactive_users_count)
			if err != nil {
				log.Fatal(err)
			}

			if(inactive_users_count < 1){
				continue
			}

			var inactive_users []app_models.User
			db.Find(&inactive_users, "is_inactive = true AND city_id = ?", city_id)

			pixels := []app_models.Pixel{}

			var corner_pixels []app_models.Pixel
			db.Limit(len(inactive_users)).Joins(helpers.RandomJoin("pixels WHERE is_city_corner = true AND city_id = " + strconv.Itoa(city_id))).Where("id >= random_id").Find(&corner_pixels, "is_city_corner = true AND city_id = ?", city_id)

			if(len(corner_pixels) < len(inactive_users)){
				var edge_pixels []app_models.Pixel
				db.Limit(len(inactive_users) - len(corner_pixels)).Joins(helpers.RandomJoin("pixels WHERE is_city_edge = true AND city_id = " + strconv.Itoa(city_id))).Where("id >= random_id").Find(&edge_pixels, "is_city_edge = true AND city_id = ?", city_id)
				for index, pixel := range corner_pixels {
					_ = index
					pixels = append(pixels, pixel)
				}
				for index, pixel := range edge_pixels {
					_ = index
					pixels = append(pixels, pixel)
				}
			} else {
				pixels = corner_pixels
			}

			var pixels_ids []int
			for index, pixel := range pixels {
				_ = index
				pixels_ids = append(pixels_ids, int(pixel.ID))
			}

			db.Model(app_models.Pixel{}).Where("id in (?)", pixels_ids).Updates(map[string]interface{}{"city_id": 0, "is_city_edge": false, "is_city_inner": false, "type_id": emptyPixelType.ID})

			var updated_pixels []app_models.Pixel
			db.Preload("Type").Find(&updated_pixels, "id in (?)", pixels_ids)
			for index, pixel := range updated_pixels {
				if(len(inactive_users) > index){
					user := inactive_users[index]
					pixel_room.AddPixel(int(user.ID), pixel)
					user.AddLog(db, "inactive_users_penalty", pixel)

					square_neighbors := pixel.GetNeighborsBigSquare(db, uint(city_id))
					for index, neighbor := range square_neighbors {
						_ = index
						neighbor.CheckCityEdge(db)
					}
				}
			}

			var city app_models.City
			db.Preload("Owner").First(&city, city_id)
			city.CachePixelsCount(db, city.Owner)
		}
	}
}